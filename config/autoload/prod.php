<?php

return array(
    'db' => array(
        'name' => 'kijiji',
        'host' => '127.0.0.1',
        'options' => array()
    ),
    
    'cache' => array(
        'type' => 'memcached',
        'options' => array(
            'servers'   => array(
                array('host' => 'localhost', 'port' => '11211'),
            ),
            'namespace' => 'KIJIJI2',
            'ttl'       => 3600,
        )
    ),
);