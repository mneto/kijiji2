<?php
namespace Listing;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    /**
     * get Service Config
     * 
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Listing\Category\Dao' =>  function($sm) {
                    $db = $sm->get('Listing\Db\Adapter');
                    return new \Listing\Category\Dao($db);
                },
                        
                'Listing\User\Dao' =>  function($sm) {
                    $db = $sm->get('Listing\Db\Adapter');
                    return new \Listing\User\Dao($db);
                },
                        
                'Listing\Ad\Dao' =>  function($sm) {
                    $db = $sm->get('Listing\Db\Adapter');
                    return new \Listing\Ad\Dao($db);
                },
                        
                'Listing\Category\Cache' =>  function($sm) {
                    $adapter = $sm->get('Listing\Cache\Adapter');
                    return new \Listing\Category\Cache($adapter);
                },
                
                'Listing\Cache\Adapter' =>  'Listing\Cache\AdapterFactory',
                        
                'Listing\Ad\Cache' =>  function($sm) {
                    $adapter = $sm->get('Listing\Cache\Adapter');
                    return new \Listing\Ad\Cache\Service($adapter);
                },
                        
                'Listing\Category\Service' =>  function($sm) {
                    $dao = $sm->get('Listing\Category\Dao');
                    $mapper = new \Listing\Category\Mapper();
                    $cache = $sm->get('Listing\Category\Cache');
                    $categoryService = new \Listing\Category\Service($dao, $mapper, $cache);
                    $categoryService->attachObserver($cache);
                    
                    return $categoryService;
                },
                        
                'Listing\User\Service' =>  function($sm) {
                    $dao = $sm->get('Listing\User\Dao');
                    $mapper = new \Listing\User\Mapper();
                    $validator = new \Zend\Validator\EmailAddress();
                    $crypt = new \Zend\Crypt\Password\Bcrypt();
                    return new \Listing\User\Service(
                            $dao, 
                            $mapper, 
                            $validator,
                            $crypt
                            );
                },
                        
                'Listing\Ad\Service' =>  function($sm) {
                    $dao = $sm->get('Listing\Ad\Dao');
                    $categoryService = $sm->get('Listing\Category\Service');
                    $userService = $sm->get('Listing\User\Service');
                    $cache = $sm->get('Listing\Ad\Cache');
                    
                    $mapper = new \Listing\Ad\Mapper($userService, 
                            $categoryService,
                            new \Listing\Ad\Mapper\Factory());
                    
                    $adService = new \Listing\Ad\Service($dao, 
                            $categoryService, 
                            $userService, 
                            $mapper,
                            $cache);
                    
                    $adService->attachObserver($cache);
                    return $adService;
                }
                )
        );
    }
}
