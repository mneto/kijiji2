<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Listing\Controller\Listing' => 'Listing\Controller\ListingController',
            'Listing\Controller\Index' => 'Listing\Controller\IndexController',
            ),
        ),
    
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Listing\Controller\Index',
                        'action'     => 'index'
                    )
                ),
            ),
            'ad' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/listing[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Listing\Controller\Listing',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            'listing' => __DIR__ . '/../view',
            ),
        ),
    );