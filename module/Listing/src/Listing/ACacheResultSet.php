<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * Cached version of the result set
 */
class ACacheResultSet extends AResultSet
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var array 
     */
    protected $list;
    
    /**
     * Constructor
     * 
     * @param array $list
     * @return void
     */
    public function __construct(&$list)
    {
        $this->list = $list;
    }
    
    /**
     * Count the number of elements
     * 
     * @return integer the number of elements
     */
    public function count()
    {
        return count($this->list);
    }

    /**
     * Get the current element
     * 
     * @return mixed
     */
    public function current()
    {
        return current($this->list);
    }

    /**
     * Get the key of the current element
     * 
     * @return mixed
     */
    public function key()
    {
        return key($this->list);
    }

    /**
     * Get the next element
     * 
     * @return mixed
     */
    public function next()
    {
        return next($this->list);
    }

    /**
     * Make the list point to the first element
     * 
     * @return true
     */
    public function rewind()
    {
        return reset($this->list);
    }

    /**
     * Find if the current position is still valid
     * 
     * @return boolean
     */
    public function valid()
    {
        $key = key($this->list);
        if (isset($this->list[$key])) {
            return true;
        } else {
            return false;
        }
    }
}
