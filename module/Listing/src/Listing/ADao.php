<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * Abastract Dao class for mongo based
 */
abstract class ADao
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \MongoDB 
     */
    protected $db;
    
    /**
     *
     * @var \MongoCollection
     */
    protected $collection;
    
    /**
     * Get the mongo db connector
     * 
     * @return \MongoDB
     * @codeCoverageIgnore
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Set the mongo db connector
     * 
     * @param \MongoDB $db the mongo db connector
     * @return void
     * @codeCoverageIgnore
     */
    public function setDb($db)
    {
        $this->db = $db;
    }
    
    /**
     * Get the mongo collection
     * 
     * @return \MongoCollection
     * @codeCoverageIgnore
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set the mongo collection
     * 
     * @param \MongoCollection $collection
     * @return void
     * @codeCoverageIgnore
     */
    public function setCollection(\MongoCollection $collection)
    {
        $this->collection = $collection;
    }
    
    /**
     * Constructor
     * 
     * @param \MongoDB $db the mongo db connector
     * @return void
     * @codeCoverageIgnore
     */
    public function __construct(\MongoDB $db)
    {
        $this->db = $db;
    }
}
