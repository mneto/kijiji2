<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * Resultset for mongo based dao
 */
class ADaoResultSet implements \Countable, \Iterator
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \MongoCursor 
     */
    protected $cursor;
    
    /**
     *
     * @var \MongoCollection 
     */
    protected $collection;
    
    /**
     *
     * @var string 
     */
    protected $query;

    /**
     * Constructor
     * 
     * @param string $query
     * @param \MongoCollection $collection
     * @param \MongoCursor $cursor
     */
    public function __construct(
        $query,
        \MongoCollection $collection,
        \MongoCursor $cursor
    ) {
        $this->query = $query;
        $this->cursor = $cursor;
        $this->collection = $collection;
    }
 
    /**
     * Get the number of elements
     * 
     * @return integer
     */
    public function count()
    {
        // Use the true to use the skip/limit if set
        return $this->cursor->count(true);
    }

    /**
     * Get the current element
     * 
     * @return array
     */
    public function current()
    {
        return $this->cursor->current();
    }

    /**
     * Get the key of the current element
     * 
     * @return mixed
     */
    public function key()
    {
        return $this->cursor->key();
    }

    /**
     * Get the next element
     * 
     * @return array
     */
    public function next()
    {
        return $this->cursor->next();
    }

    /**
     * Returns the result set to the first element
     * 
     * @return true
     */
    public function rewind()
    {
        return $this->cursor->rewind();
    }

    /**
     * Validates that the current is valid
     * 
     * @return boolean
     */
    public function valid()
    {
        return $this->cursor->valid();
    }
}
