<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * Abastract Mapper
 */
abstract class AMapper implements IMapper
{
    abstract function getClassName();
    
    /**
     * @param \StdClass $obj The object to map
     * @return array
     */
    public function mapToDb($obj)
    {
        $className = $this->getClassName();
        if ($obj instanceof $className) {
            $details = array();
            
            $reflection = new \ReflectionClass($obj);
            $properties = $reflection->getProperties();
            foreach ($properties as $property) {
                if ($property->getName() == 'id') {
                    $details['_id'] = new \MongoId($obj->getId());
                } else {
                    $method = 'get' . ucfirst($property->getName());
                    $details[$property->getName()] = $obj->{$method}();
                }
            }
            
            return $details;
        }
    }

    /**
     * Map a DB result to an object
     * 
     * @param array $data
     * @param array $options
     * @return \StdClass
     */
    public function mapToObject(array &$data, array $options = array())
    {
        if (!strlen($this->getClassName())) {
            throw new Exception\InvalidParameter('Class name not defined');
        }
  
        $className = $this->getClassName();
        $obj = new $className;
        $reflection = new \ReflectionClass($obj);
        $properties = $reflection->getProperties();
        foreach ($properties as $property) {
            // the id is special
            if ($property->getName() == 'id') {
                $obj->setId($data['_id']->{'$id'});
            } else if (isset($data[$property->getName()])) {
                    $method = 'set' . ucfirst($property->getName());
                    $obj->{$method}($data[$property->getName()]);
            }
        }
        
        return $obj;
    }
}
