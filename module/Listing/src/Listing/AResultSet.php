<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

use \Listing\ADaoResultSet;
use \Listing\IMapper;

/**
 * Abstract result set to be used as base for all services that results elements
 */
class AResultSet implements \Countable, \Iterator
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \Listing\ADaoResultSet
     */
    protected $daoResultSet;
    
    /**
     *
     * @var \Listing\IMapper
     */
    protected $mapper;

    /**
     * 
     * @param \Listing\ADaoResultSet $daoResultSet
     * @param \Listing\IMapper $mapper
     */
    public function __construct(ADaoResultSet $daoResultSet, IMapper $mapper)
    {
        $this->daoResultSet = $daoResultSet;
        $this->mapper = $mapper;
    }

    /**
     * Get the number of elements
     * 
     * @return integer
     */
    public function count()
    {
        return $this->daoResultSet->count();
    }

    /**
     * Return the current element from the DAO set
     * 
     * @return \StdClass
     */
    public function current()
    {
        $data = $this->daoResultSet->current();
        return $this->mapper->mapToObject($data);
    }

    /**
     * Return the key associated with the current element
     * 
     * @return mixed
     */
    public function key()
    {
        return $this->daoResultSet->key();
    }

    /**
     * Return the next element
     * 
     * @return \StdClass
     */
    public function next()
    {
        return $this->daoResultSet->next();
    }

    /**
     * Make the set point back to the first element
     * 
     * @return boolean
     */
    public function rewind()
    {
        return $this->daoResultSet->rewind();
    }

    /**
     * Validates that the element I want to access is valid
     * 
     * @return boolean
     */
    public function valid()
    {
        return $this->daoResultSet->valid();
    }
}
