<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

use \Listing\Category;
use \Listing\User;
use \Listing\Ad\ADetail;

/**
 * Ad entity
 */
class Ad
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Enabled AD
     */
    const STATUS_ENABLED = 'enabled';
    
    /**
     * Disabled AD
     */
    const STATUS_DISABLED = 'disabled';
    
    /**
     * AD for auto
     */
    const TYPE_AUTO = 'auto';
    
    /**
     * Ad for houses
     */
    const TYPE_HOUSING = 'housing';
    
    /**
     *
     * @var integer 
     */
    protected $id;
    
    /**
     *
     * @var string 
     */
    protected $title;
    
    /**
     *
     * @var \Listing\Category 
     */
    protected $category;
    
    /**
     *
     * @var \Listing\User 
     */
    protected $owner;
    
    /**
     *
     * @var integer 
     */
    protected $createDate;
    
    /**
     *
     * @var string 
     */
    protected $status;
    
    /**
     *
     * @var string 
     */
    protected $type;
    
    /**
     *
     * @var float 
     */
    protected $price;
    
    /**
     *
     * @var \Listing\Ad\Image[] 
     */
    protected $images = array();
    
    /**
     *
     * @var \Listing\Ad\ADetail 
     */
    protected $details;
    
    /**
     * Get the id
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the id
     * 
     * @param string $id
     * @return void
     * @codeCoverageIgnore
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the title
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the title
     * 
     * @param string $title
     * @codeCoverageIgnore
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get the category of the Ad
     * 
     * @return \Listing\Category
     * @codeCoverageIgnore
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the category
     * 
     * @param \Listing\Category $category
     * @return void
     * @codeCoverageIgnore
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get the user that owns the ad
     * 
     * @return \Listing\User
     * @codeCoverageIgnore
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set the owner
     * 
     * @param \Listing\User $owner
     * @return void
     * @codeCoverageIgnore
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * Get the creation date
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set the creation date
     * 
     * @param string $createDate
     * @return void
     * @codeCoverageIgnore
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * Get the status
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the status
     * 
     * @param string $status
     * @codeCoverageIgnore
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get the type of the AD
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the type
     * 
     * @param string $type
     * @return void
     * @codeCoverageIgnore
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get the price
     * 
     * @return float
     * @codeCoverageIgnore
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the price
     * 
     * @param float $price
     * @codeCoverageIgnore
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get the list of images
     * 
     * @return \Listing\Ad\Image[]
     * @codeCoverageIgnore
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set the list of images
     * 
     * @param \Listing\Ad\Image[] $images
     * @return void
     * @codeCoverageIgnore
     */
    public function setImages(array $images)
    {
        $this->images = $images;
    }

    /**
     * Get the details
     * 
     * @return \Listing\Ad\ADetail
     * @codeCoverageIgnore
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set the details
     * 
     * @param \Listing\Ad\ADetail $details
     * @return void
     * @codeCoverageIgnore
     */
    public function setDetails(ADetail $details)
    {
        $this->details = $details;
    }
}
