<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */ 

namespace Listing\Ad;

/**
 * Detail of type Auto
 */
class Auto extends ADetail
{
    /**
     *
     * @var string 
     */
    protected $brand;
    
    /**
     *
     * @var string 
     */
    protected $year;
    
    /**
     * Get the brand
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getBrand() 
    {
        return $this->brand;
    }

    /**
     * Set the brand
     * 
     * @param strind $brand
     * @return void
     * @codeCoverageIgnore
     */
    public function setBrand($brand) 
    {
        $this->brand = $brand;
    }

    /**
     * Get the year
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the year
     * 
     * @param string $year
     * @return void
     * @codeCoverageIgnore
     */
    public function setYear($year)
    {
        $this->year = $year;
    }
}
