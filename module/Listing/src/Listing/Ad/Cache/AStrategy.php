<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Cache;

use Listing\Cache\Strategy;
use Listing\AResultSet;

/**
 * Cache strategy for AD
 */
abstract class AStrategy extends Strategy 
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;  
    
    /**
     * The main tag to use 
     */
    const MAIN_TAG = 'AD_CATEGORY';
    
    /**
     * Builds a key
     * 
     * @param string $categoryId
     * @param array $options
     * @return string
     */
    public function buildKey($categoryId, &$options) 
    {
        if (!isset($options['status'])) {
            throw new \Listing\Exception\InvalidParameter('The status is missing');
        }

        if (!isset($options['limit'])) {
            throw new \Listing\Exception\InvalidParameter('The limit is missing');
        }

        if (!isset($options['offset'])) {
            throw new \Listing\Exception\InvalidParameter('The offset is missing');
        }

        return self::MAIN_TAG
               . '_' . $categoryId
               . '_' . $options['status'] 
               . '_' . $options['limit'] 
               . '_' . $options['offset'];
    }
    
    /**
     * Builds the tag associated with a list of ads by a category
     * 
     * @param string $categoryId
     * @return string
     */
    public function buildListTags($categoryId) 
    {
        return self::MAIN_TAG . '_' . $categoryId;
    }
    
    /**
     * Save the category list
     * 
     * @param \Listing\AResultSet $list
     * @param type $categoryId
     * @param type $options
     * @return boolean
     */
    abstract public function saveList(AResultSet $list, $categoryId, &$options);
    
    /**
     * Get the category list
     * 
     * @param string $categoryId
     * @param array $options
     * @return boolean|\Listing\ACacheResultSet
     */
    abstract public function getList($categoryId, $options);
    
    /**
     * Removes the list for a given category id
     * 
     * @param string $categoryId
     */
    abstract public function eraseList($categoryId);
}
