<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Cache;

use Listing\Ad\Cache\AStrategy;
use Listing\AResultSet;
use Listing\ACacheResultSet;
use Listing\Observable;

/**
 * Service based implementation
 */
class Service extends AStrategy
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;        
    
    /**
     * Save the category list
     * 
     * @param \Listing\AResultSet $list
     * @param string $categoryId
     * @param array $options
     * @return boolean
     */
    public function saveList(AResultSet $list, $categoryId, &$options) 
    {
        $key = $this->buildKey($categoryId, $options);
        $newList = array();
        foreach ($list as $item) {
            $newList[] = $item;
        }
        $this->adapter->setItem($key, $newList);
        return $this->adapter->setTags($key, array($this->buildListTags($categoryId)));
    }
    
    /**
     * Get the category list
     * 
     * @param string $categoryId
     * @param array $options
     * @return boolean|\Listing\ACacheResultSet
     */
    public function getList($categoryId, $options) 
    {
        $list = $this->adapter->getItem($this->buildKey($categoryId, $options));
        if (is_array($list)) {
            return new ACacheResultSet($list);
        } else {
            return false;
        }
    }

    /**
     * Removes the list for a given category id
     * 
     * @param string $categoryId
     * @return boolean
     */
    public function eraseList($categoryId) 
    {
        // I'd have to located the list this category Id belongs and erase it.
        $tag = $this->buildListTags($categoryId);
        return $this->adapter->clearByTags(array($tag));
    }
    
    /**
     * Updates the cache
     * 
     * @param \Listing\Observable $observable
     * @param string $action
     */
    public function update(Observable $observable, $action) 
    {
        if ($observable instanceof \Listing\Ad\Service) {
            $ad = $observable->getAd();
            $this->eraseList($ad->getCategory()->getId());
        }
    }
}
