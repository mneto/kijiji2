<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad;

use Listing\ADao;
use Listing\Ad\Dao\ITemplate;
use Listing\Ad\Dao\ResultSet;

class Dao extends ADao implements ITemplate 
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Collection name
     */
    const COLLECTION = 'ads';

    /**
     *
     * @var \MongoCollection 
     */
    protected $collection;
    
    /**
     * Constructor
     * 
     * @param \MongoDB $db
     * @return void
     * @codeCoverageIgnore
     */
    function __construct(\MongoDB $db)
    {
        parent::__construct($db);
        $this->collection = new \MongoCollection ($this->db, self::COLLECTION);
    }
    
    /**
     * Insert one AD
     * 
     * @param array $data
     * @return array the same data added with the id set
     */
    public function add(array $data)
    {
        $this->collection->insert($data);
        return $data;
    }

    /**
     * Remove one Ad
     * 
     * @param string $adId
     * @return boolean
     */
    public function delete($adId)
    {
        $rc = $this->collection->remove(array('_id' => new \MongoId($adId)));
        if ($rc['n'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns one AD
     * 
     * @param string $id the ad id
     * @param array $options
     * @return array|null
     */
    public function searchById($id, array $options = array())
    {
        return $this->collection->findOne(array('_id' => new \MongoId($id)));
    }
    
    /**
     * Get all ads of a given category
     * 
     * @param string $categoryId
     * @param array $options
     * @return \Listing\Ad\Dao\ResultSet|null
     */
    public function searchByCategory($categoryId, array $options = array())
    {
        $query = array('categoryId' => $categoryId);
        
        if(isset($options['count']) && $options['count']) {
            return $this->collection->count($query);
        }
        
        $cursor = $this->collection->find($query);
        
        if(isset($options['limit'])) {
            $cursor->limit($options['limit']);
        }
        
        if(isset($options['offset'])) {
            $cursor->skip($options['offset']);
        }
                
        return new ResultSet($query, $this->collection, $cursor);
    }
    
    /**
     * Get the list of category ids and the number of ads in each
     * 
     * @param array $options the list of options<br />
     * - status string the status of the ads to be returned in the list
     * 
     * @return array (categoryId    => string, 
     *                numberOfItems => integer)
     */
    public function getNumberOfAdsPerCategory(array $options = array())
    {
        $pipeline = array();
        
        if(isset($options['status'])) {
            $pipeline[] = array(
                '$match' => array(
                    'status' => $options['status']
                )
            );
        }
        
        $pipeline[] = 
            array('$group' => array(
                '_id' => array('categoryId' => '$categoryId'),
                'numberOfItems' => array('$sum' => 1)
            ));
        
        $pipeline[] = 
            array('$sort' => array('numberOfItems' => 1));
        
        $rc = $this->collection->aggregate($pipeline);
        if(isset($rc['result']) && count($rc['result'])) {
            $list = array();
            foreach ($rc['result'] as $item) {
                $list[] = array('categoryId' => $item['_id']['categoryId'],
                                'numberOfItems' => $item['numberOfItems']);
            }
            return $list;
        } else {
            return null;
        }
        
    }

    /**
     * Update one ad
     * 
     * @param array $data the ad to be updated
     * @return boolean true if successful and false otherwise
     */
    public function update(array $data)
    {
        $rc = $this->collection->update(array('_id' => $data['_id']), $data);

        if($rc['n']) {
            return true;
        } else {
            return false;
        }
    }
}
