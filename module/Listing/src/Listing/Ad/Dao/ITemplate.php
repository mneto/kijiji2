<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Dao;

/**
 * Dao interface
 */
interface ITemplate 
{
    /**
     * Get all ads of a given category
     * 
     * @param string $categoryId
     * @param array $options
     * @return \Listing\Ad\Dao\ResultSet|null
     */
    public function searchByCategory($categoryId, array $options = array());
    
    /**
     * Get the list of category ids and the number of ads in each
     * 
     * @param array $options the list of options<br />
     * - status string the status of the ads to be returned in the list
     * 
     * @return array (categoryId    => string, 
     *                numberOfItems => integer)
     */
    public function getNumberOfAdsPerCategory(array $options = array());
    
    /**
     * Insert one AD
     * 
     * @param array $data
     * @return array the same data added with the id set
     */
    public function add(array $data);
    
    /**
     * Returns one AD
     * 
     * @param string $id the ad id
     * @param array $options
     * @return array|null
     */
    public function searchById($id, array $options = array());
    
    /**
     * Remove one Ad
     * 
     * @param string $adId
     * @return boolean
     */
    public function delete($adId);
    
    /**
     * Update one ad
     * 
     * @param array $data the ad to be updated
     * @return boolean true if successful and false otherwise
     */
    public function update(array $data);
}
