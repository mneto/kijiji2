<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Dao;

use Listing\ADaoResultSet;

/**
 * Result set for DAO access specific for ADs
 */
class ResultSet extends ADaoResultSet 
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
}
