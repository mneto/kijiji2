<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad;

/**
 * Housing details
 */
class Housing extends ADetail {
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var integer 
     */
    protected $numberOfRooms;
    
    /**
     *
     * @var integer 
     */
    protected $numberOfBathrooms;
    
    /**
     * Get the number of rooms
     * 
     * @return integer
     */
    public function getNumberOfRooms()
    {
        return $this->numberOfRooms;
    }

    /**
     * Set the number of rooms
     * 
     * @param integer $numberOfRooms
     * @return void
     */
    public function setNumberOfRooms($numberOfRooms)
    {
        $this->numberOfRooms = $numberOfRooms;
    }

    /**
     * Set the number of bathrooms
     * 
     * @return integer
     */
    public function getNumberOfBathrooms()
    {
        return $this->numberOfBathrooms;
    }

    /**
     * Set the number of bathrooms
     * 
     * @param integer $numberOfBathrooms
     * @return void
     */
    public function setNumberOfBathrooms($numberOfBathrooms)
    {
        $this->numberOfBathrooms = $numberOfBathrooms;
    }
}
