<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad;

use Listing\Ad;
use Listing\Ad\Mapper\ITemplate;

/**
 * The mapper for the main ad
 */
class Mapper implements ITemplate
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \Listing\Ad\Mapper\Factory
     */
    protected $mapperFactory;
    
    /**
     *
     * @var \Listing\Category\Service\ITemplate
     */
    protected $categoryService;

    /**
     *
     * @var \Listing\User\Service\ITemplate 
     */
    protected $userService;

    /**
     * Constructor
     * 
     * @param \Listing\User\Service\ITemplate $userService
     * @param \Listing\Category\Service\ITemplate $categoryService
     * @param \Listing\Ad\Mapper\Factory $mapperFactory
     * @return void
     */
    public function __construct(\Listing\User\Service\ITemplate $userService, 
            \Listing\Category\Service\ITemplate $categoryService, 
            \Listing\Ad\Mapper\Factory $mapperFactory)
    {
        $this->userService = $userService;
        
        $this->categoryService = $categoryService;
        
        $this->mapperFactory = $mapperFactory;
    }
    
    /**
     * Map the data array to the object
     * 
     * @param array $data
     * @param array $options
     * @return \Listing\Ad
     */
    public function mapToObject(array &$data, array $options = array())
    {
        $ad = new \Listing\Ad();
        $ad->setId($data['_id']->{'$id'});
        
        if (isset($data['title'])) {
            $ad->setTitle($data['title']);
        }
        
        $ad->setCategory($this->categoryService->searchById($data['categoryId']));
        $ad->setOwner($this->userService->searchById($data['userId']));
        $ad->setStatus($data['status']);
        $ad->setType($data['type']);
        
        if (isset($data['createDate'])) {
            $ad->setCreateDate($data['createDate']);
        }
        $ad->setPrice($data['price']);

        $mapper = $this->mapperFactory->create($data['type']);
        $ad->setDetails($mapper->mapToObject($data['details']));

        return $ad;
    }
    
    /**
     * Maps the object to array
     * 
     * @param \Listing\Ad $obj
     * @return array
     */
    public function mapToDb($obj)
    {
        if ($obj instanceof Ad) {
           $data = array();
           $data['_id'] = new \MongoId($obj->getId());
           $data['title'] = $obj->getTitle();
           $data['categoryId'] = $obj->getCategory()->getId();
           $data['createDate'] = $obj->getCreateDate();
           $data['userId'] = $obj->getOwner()->getId();
           $data['price'] = $obj->getPrice();
           $data['status'] = $obj->getStatus();
           $data['type'] = $obj->getType();
           
           $mapper = $this->mapperFactory->create($data['type']);
           $data['details'] = $mapper->mapToDb($obj->getDetails());
           
           return $data;
        }
    }
}
