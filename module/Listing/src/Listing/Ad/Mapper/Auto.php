<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Mapper;

use Listing\AMapper;

/**
 * Mapper for auto
 */
class Auto extends AMapper 
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Get the class name to instantiate
     * 
     * @return string
     */
    public function getClassName()
    {
        return '\Listing\Ad\Auto';
    }
}
