<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Mapper;

use \Listing\Ad;
use \Listing\Ad\InvalidTypeException;

/**
 * Factory mapper for ad details
 */
class Factory {
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * 
     * @param string $type
     * @param array $options
     * @return Listing\Ad\Mapper\ITemplate
     */
    public function create($type, array $options=array()) 
    {
        switch ($type) {
            case Ad::TYPE_AUTO:
                return new Auto();
                break;
            
            case Ad::TYPE_HOUSING:
                return new Housing();
                break;
            
            default :
                throw new InvalidTypeException($type . ' is invalid');
        }
    }
}
