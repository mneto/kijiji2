<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Mapper;

use Listing\AMapper;

/**
 * Mapping for housing details
 */
class Housing extends AMapper
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Get the class name to instantiate
     * 
     * @return string
     */
    public function getClassName()
    {
        return '\Listing\Ad\Housing';
    }
}
