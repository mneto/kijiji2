<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Mapper;

use Listing\IMapper;

/**
 * Mapper interface for Ad
 */
interface ITemplate extends IMapper {
    
}
