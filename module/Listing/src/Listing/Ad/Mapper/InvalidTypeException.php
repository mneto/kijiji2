<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Mapper;

/**
 * Invalid type of exception
 * 
 */
class InvalidTypeException extends \Exception
{
    
}
