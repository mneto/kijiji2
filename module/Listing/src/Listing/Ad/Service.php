<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad;

use Listing\AResultSet;
use Listing\Observable;
use Listing\Observer;

/**
 * Ad service
 */
class Service implements Service\ITemplate, Observable
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \Listing\Ad\Dao 
     */
    protected $dao;
    
    /**
     *
     * @var \Listing\Category\Service\ITemplate
     */
    protected $categoryService;
    
    /**
     *
     * @var \Listing\User\Service\ITemplate 
     */
    protected $userService;
    
    /**
     *
     * @var \Listing\Ad\Mapper\ITemplate
     */
    protected $mapper;
    
    /**
     *
     * @var \Listing\Ad\Cache\AStrategy
     */
    protected $cache;
    
    /**
     *
     * @var \Listing\Ad
     */
    protected $ad;
    
    /**
     *
     * @var \Listing\Observer[] 
     */
    protected $observers = array();

    /**
     * Get the list of observers
     *
     * @return array|\Listing\Observer[]
     * @codeCoverageIgnore
     */
    public function getObservers()
    {
        return $this->observers;
    }
    
    /**
     * @codeCoverageIgnore
     * @return Listing\Ad\Dao
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * @codeCoverageIgnore
     * @param \Listing\Ad\Dao\ITemplate $dao
     */
    public function setDao(Dao\ITemplate $dao)
    {
        $this->dao = $dao;
    }

    /**
     * @codeCoverageIgnore
     * @return \Listing\Category\Service\ITemplate
     */
    public function getCategoryService()
    {
        return $this->categoryService;
    }

    /**
     * @codeCoverageIgnore
     * @param \Listing\Category\Service\ITemplate $categoryService
     */
    public function setCategoryService(
            \Listing\Category\Service\ITemplate $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    
    /**
     *
     * @codeCoverageIgnore
     * @return \Listing\User\Service\ITemplate
     */
    public function getUserService() {
        return $this->userService;
    }

    /**
     *
     * @codeCoverageIgnore
     * @return \Listing\Ad\Mapper\ITemplate
     */
    public function getMapper() {
        return $this->mapper;
    }

    /**
     *
     * @codeCoverageIgnore
     * @return \Listing\Ad\Cache\AStrategy
     */
    public function getCache() {
        return $this->cache;
    }

    /**
     *
     * @codeCoverageIgnore
     * @param \Listing\User\Service\ITemplate $userService
     * @return void
     */
    public function setUserService(
            \Listing\User\Service\ITemplate $userService)
    {
        $this->userService = $userService;
    }

    /**
     *
     * @codeCoverageIgnore
     * @param \Listing\Ad\Mapper\ITemplate $mapper
     * @return void
     */
    public function setMapper(\Listing\Ad\Mapper\ITemplate $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     *
     * @codeCoverageIgnore
     * @param \Listing\Ad\Cache\AStrategy $cache
     * @return void
     */
    public function setCache(\Listing\Ad\Cache\AStrategy $cache)
    {
        $this->cache = $cache;
    }
    
    /**
     *
     * @codeCoverageIgnore
     * @return \Listing\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     *
     * @codeCoverageIgnore
     * @param \Listing\Ad $ad
     * @return void
     */
    public function setAd(\Listing\Ad $ad) {
        $this->ad = $ad;
    }

    /**
     * Constructor
     * 
     * @param \Listing\Ad\Dao\ITemplate $dao
     * @param \Listing\Category\Service\ITemplate $categoryService
     * @param \Listing\User\Service\ITemplate $userService
     * @param \Listing\Ad\Mapper\ITemplate $mapper
     * @param \Listing\Ad\Cache\AStrategy $cache
     * @return void
     * @codeCoverageIgnore
     */
    function __construct(Dao\ITemplate $dao,
            \Listing\Category\Service\ITemplate $categoryService,
            \Listing\User\Service\ITemplate $userService,
            \Listing\Ad\Mapper\ITemplate $mapper,
            \Listing\Ad\Cache\AStrategy $cache)
    {
        
        $this->dao = $dao;
        
        $this->categoryService = $categoryService;
        
        $this->userService = $userService;
        
        $this->mapper = $mapper;
        
        $this->cache = $cache;
    }
    
    /**
     * Adds one AD
     * 
     * @param array $data
     * @return \Listing\Ad|null
     */
    public function add(array $data)
    {
        $filter = new \Zend\Filter\StringToUpper();
        
        $data['createDate'] = time();
        $data['title'] = $filter->filter($data['title']);
        $rc = $this->dao->add($data);
        
        // Map to the Object
        if(is_array($rc)) {
            $ad = $this->mapper->mapToObject($rc);
            $this->ad = $ad;
            $this->notify('add');
            return $ad;
        } else {
            return null;
        }
    }

    /**
     * Delete an ad
     * 
     * @param string $adId
     * @return boolean
     */
    public function delete($adId)
    {
        if (!strlen($adId)) {
            return false;
        }
        
       $rc = $this->dao->delete($adId);
       
       return $rc;
    }

    /**
     * Get one ad by id
     * 
     * @param string $id
     * @param array $options
     * @return \Listing\Ad|null
     */
    public function searchById($id, array $options = array())
    {
        $rc = $this->dao->searchById($id, $options);
        if(is_array($rc)) {
            return $this->mapper->mapToObject($rc);
        } else {
            return null;
        }
    }
    
    /**
     * Get the list of Ads from a given category
     * 
     * @param integer $categoryId
     * @param array $options the options<br />
     * - count boolean if true only returns the number of elements<br />
     * - status string the status of the ads I want to filter<br />
     * - limit integer the number of elements to return<br />
     * - offset integer the initial
     * @return \Listing\AResultSet
     */
    public function searchByCategory($categoryId, array $options = array())
    {
       if(!isset($options['status'])) {
           $options['status'] = \Listing\Ad::STATUS_ENABLED;
       }
       
       if(!isset($options['limit'])) {
           $options['limit'] = 10;
       }
       
       if(!isset($options['offset'])) {
           $options['offset'] = 0;
       }
       
       // I just want the number of all ads in a given category
       if (isset($options['count']) && $options['count']) {
           return $this->dao->searchByCategory($categoryId, $options);
       }
       
       $rc = $this->cache->getList($categoryId, $options);
       if ($rc === false) {
           $rc = $this->dao->searchByCategory($categoryId, $options);
           if($rc != null) {
               $list = new AResultSet($rc, $this->mapper);
               $this->cache->saveList($list, $categoryId, $options);
               return $list;
           } else {
                return null;
           }
       } else {
           return $rc;
       }
    }
    
    /**
     * Get the number of Ads per category
     * 
     * @param array $options options<br />
     * - status string
     * 
     * @return array (category => \Listing\Category,
     *                numberOfItems => integer)
     */
    public function getNumberOfAdsPerCategory(array $options = array())
    {
        $rc = $this->dao->getNumberOfAdsPerCategory($options);
        if (is_array($rc)) {
            $list = array();
            foreach ($rc as $item) {
                $category = $this->categoryService->searchById($item['categoryId']);
                $list[] = array('category'      => $category,
                                'numberOfItems' => $item['numberOfItems']);
            }
            $rc = $list;
        }
        
        return $rc;
    }

    /**
     * Update the ad
     * 
     * @param \Listing\Ad $ad the ad to be updated
     * @return boolean
     */
    public function update(\Listing\Ad $ad)
    {
       // Saving for later
       $this->ad = $ad;
       
       if ($ad->getId() == NULL) {
           return false;
       } 
            
       $rc = $this->dao->update($this->mapper->mapToDb($ad));
       if ($rc) {
          $this->notify('update'); 
       }
       
       return $rc;
    }
    
    /**
     * Attach the observer
     * 
     * @param \Listing\Observer $observer
     * @return \Listing\Ad\Service
     */
    public function attachObserver(Observer $observer)
    {
        $attach = true;
        foreach ($this->observers as $current) {
            if (get_class($observer) == get_class($current)) {
                $attach = false;
                break;
            }
        }
        
        if ($attach) {
            $this->observers[] = $observer;
        }
        
        return $this;
    }

    /**
     * 
     * @param \Listing\Observer $observer
     * @return \Listing\Ad\Service
     */
    public function detachObserver(Observer $observer)
    {
        foreach ($this->observers as $key => $current) {
            if (get_class($observer) == get_class($current)) {
                unset($this->observers[$key]);
                break;
            }
        }
        return $this;
    }

    /**
     * Notify
     * 
     * @param string $action
     * @return \Listing\Ad\Service
     */
    public function notify($action)
    {
        foreach ($this->observers as $observer) {
            $observer->update($this, $action);
        }
        
        return $this;
    }
}
