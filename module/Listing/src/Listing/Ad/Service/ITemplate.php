<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Ad\Service;

use Listing\Ad;

/**
 * Interface for ad service
 */
interface ITemplate {
    /**
     * Add one ad
     * 
     * @param array $data
     * @return \Listing\Ad
     */
    public function add(array $data);
    
    /**
     * Update one ad
     * 
     * @param \Listing\Ad $ad
     * @return boolean
     */
    public function update(Ad $ad);
    
    /**
     * Delete the ad
     * 
     * @param string $adId
     * @return boolean
     */
    public function delete($adId);
    
    /**
     * Search one ad by Id
     * 
     * @param string $id
     * @param array $options
     * @return \Listing\Ad The ad
     */
    public function searchById($id, array $options = array());
   
    /**
     * Get the list of Ads from a given category
     * 
     * @param integer $categoryId
     * @param array $options the options<br />
     * - count boolean if true only returns the number of elements<br />
     * - status string the status of the ads I want to filter<br />
     * - limit integer the number of elements to return<br />
     * - offset integer the initial
     */
    public function searchByCategory($categoryId, array $options = array());
     
    /**
     * Get the number of Ads per category
     * 
     * @param array $options options<br />
     * - status string
     * 
     * @return array (category => \Listing\Category,
     *                numberOfItems => integer)
     */
    public function getNumberOfAdsPerCategory(array $options = array());
}
