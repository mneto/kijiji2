<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Cache;

use Listing\Cache\Exception;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Cache\StorageFactory;

/**
 * Factory for the zend\cache\adapter
 */
class AdapterFactory implements FactoryInterface
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Create cache adapter
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Listing\Cache\Adapter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        switch($config['cache']['type']) {
            case 'filesystem':
                $storage = StorageFactory::factory(array(
                        'adapter' => array(
                            'name' => $config['cache']['type'],
                            'options' => $config['cache']['options'],
                        ),
                        'plugins' => array(
                            'exception_handler' => array('throw_exceptions' => false),
                            'serializer',
                        ),
                        ));

                $adapter = new \Listing\Cache\Adapter($storage);
                break;
            
            case 'memcached':
                $storage = StorageFactory::factory(array(
                        'adapter' => array(
                            'name'    => $config['cache']['type'],
                            'options' => $config['cache']['options'],
                        ),
                        'plugins' => array(
                            'exception_handler' => array('throw_exceptions' => false),
                            'serializer',
                        ),
                        ));

                $adapter = new \Listing\Cache\Taggable($storage);
                break;
            
            case 'null':
                $storage = StorageFactory::factory(array(
                    'adapter' => 'memory',
                    'plugins' => array(
                        'exception_handler' => array('throw_exceptions' => false),
                        'Serializer',
                    ),
                    'options' => $config['cache']['options'],
                ));
                $adapter = new \Listing\Cache\Null($storage);
                break;
            
            default:
                throw new Exception('Invalid cache type ' . $config['cache']['type']);
        }
        return $adapter;
    }
}
