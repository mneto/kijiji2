<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Cache;

/**
 * cache exception
 *
 */
class Exception extends \Exception
{

}
