<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Cache;

/**
 * Defines our taggable interface for cache
 *
 * Class ITaggable
 * @package Listing\Cache
 */
interface ITaggable
{
    /**
     * Set the tags for a given key
     *
     * @param string $key the key
     * @param array $tags the list of tags I want to add the key to
     * @return boolean
     */
    public function setTags($key, array $tags);

    /**
     * Get the list of keys associated with a given key
     *
     * @param string $tag
     * @return array
     */
    public function getByTag($tag);

    /**
     * Remove one key from a given key
     *
     * @param string $key the key I am looking for
     * @param string $tag the tag I want it removed from
     * @return boolean
     */
    public function removeKeyFromTag($key, $tag);

    /**
     * Remove a tag
     *
     * @param string $tag
     * @return boolean
     */
    public function removeTag($tag);

    /**
     * Remove elements by the tag
     *
     * @param array $tags
     * @param boolean $disjunction
     * @return boolean
     */
    public function clearByTags(array $tags, $disjunction = false);
}