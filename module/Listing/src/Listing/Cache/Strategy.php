<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Cache;

use Listing\Cache\Adapter;
use Listing\Observer;

/**
 * Abstract cache structure
 * 
 */
abstract class Strategy implements Observer
{    
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \Listing\Cache\Adapter
     */
    protected $adapter;
    
    /**
     * 
     * @return \Listing\Cache\Adapter
     * @codeCoverageIgnore
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * 
     * @param \Listing\Cache\Adapter $adapter
     * @return void
     * @codeCoverageIgnore
     */
    public function setAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    /**
     * Constructor
     * 
     * @param \Listing\Cache\Adapter $adapter
     * @return void
     * @codeCoverageIgnore
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }
}
