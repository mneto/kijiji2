<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Cache;


class Taggable extends Adapter implements ITaggable
{
    /**
     * Set the tags for a given key
     *
     * @param string $key the key
     * @param array $tags the list of tags I want to add the key to
     * @return boolean
     */
    public function setTags($key, array $tags)
    {
        foreach ($tags as $tag) {
            // 1 phase - find if I have such tag

            if ($this->storage->hasItem($tag)) {
                $cache = $this->storage->getItem($tag);
                if (!in_array($key, $cache)) {
                    // Since I do not have the element I'll add it
                    $cache[] = $key;
                    // 2 phase - add the key to the list of keys for a given tag
                    $this->storage->replaceItem($tag, $cache);
                }
            } else {
                // I don't have it
                $cache = array($key);
                $this->storage->setItem($tag, $cache);
            }
        }
        return true;
    }

    /**
     * Get the list of keys associated with a given key
     *
     * @param string $tag
     * @return array
     */
    public function getByTag($tag)
    {
        return $this->storage->getItem($tag);
    }

    /**
     * Remove one key from a given key
     *
     * @param string $key the key I am looking for
     * @param string $tag the tag I want it removed from
     * @return boolean
     */
    public function removeKeyFromTag($key, $tag)
    {
        // 1 phase - find if I have such tag
        if ($this->storage->hasItem($tag)) {
            $keys = $this->storage->getItem($tag);
            // 2 phase - remove the key to the list of keys for a given tag
            $newKeys = array_diff($keys, array($key));
            if (count($newKeys) != count($keys)) {
                return $this->storage->replaceItem($tag, $key);
            }
        }
        return false;
    }

    /**
     * Remove a tag
     *
     * @param string $tag
     * @return boolean
     */
    public function removeTag($tag)
    {
        // 1 phase - delete the tag
        return $this->storage->removeItem($tag);
    }

    /**
     * Remove elements by the tag
     *
     * @param array $tags
     * @param boolean $disjunction
     * @return boolean
     */
    public function clearByTags(array $tags, $disjunction = false)
    {
        foreach ($tags as $tag) {
            // 1 phase - find if I have the tag
            if ($this->storage->hasItem($tag)) {
                // 2 phase - for each element remove it
                $keys = $this->getByTag($tag);
                foreach ($keys as $key) {
                    // 3 phase - unset it from the tag list
                    $this->storage->removeItem($key);
                }
                // 4 phase - remove the tag list
                $this->removeTag($tag);
            }
        }
        return true;
    }
}
