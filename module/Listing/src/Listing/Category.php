<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * Category entity
 */
class Category
{
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var integer 
     */
    protected $id;
    
    /**
     *
     * @var string 
     */
    protected $name;
    
    /**
     * Get the id
     * 
     * @return integer
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the id
     * 
     * @param integer $id
     * @return void
     * @codeCoverageIgnore
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @param string $name
     * @codeCoverageIgnore
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
