<?php

namespace Listing\Category;

use Listing\Cache\Strategy,
    Listing\AResultSet,
    Listing\ACacheResultSet,
    Listing\Observable;

class Cache extends Strategy
{
    /**
     * The base id
     */
    const CATEGORY = 'categoryList';
    
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Save the list of categories
     * 
     * @param \Listing\AResultSet $list
     * @return boolean
     */
    public function saveList(AResultSet $list)
    {
        $newList = array();
        foreach ($list as $item) {
            $newList[] = $item;
        }
        return $this->adapter->setItem(self::CATEGORY, $newList);
    }
    
    /**
     * Get a list of categories
     * 
     * @return \Listing\ACacheResultSet|false
     */
    public function getList()
    {
        $list = $this->adapter->getItem(self::CATEGORY);
        if (is_array($list)) {
            return new ACacheResultSet($list);
        } else {
            return false;
        }
    }
    
    /**
     * Removes the list of the cache
     * 
     * @return boolean
     */
    public function eraseList()
    {
        return $this->adapter->removeItem(self::CATEGORY);
    }
    
    /**
     * Receives updates from the objects I observe
     * 
     * @param \Listing\Observable $observable
     * @param string $action
     */
    public function update(Observable $observable, $action)
    {
        $this->eraseList();
    }
}
