<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Category;

use Listing\Category\Dao\ResultSet;

/**
 * Dao for category
 */
class Dao extends \Listing\ADao implements Dao\ITemplate
{
    /**
     * Name of the collection
     */
    const COLLECTION = 'categories';

    /**
     *
     * @var string 
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \MongoCollection 
     */
    protected $collection;
    
    /**
     * Constructor
     * 
     * @param \MongoDB $db
     * @return void
     * @codeCoverageIgnore
     */
    function __construct(\MongoDB $db)
    {
        parent::__construct($db);
        $this->collection = new \MongoCollection ($this->db, self::COLLECTION);
    }
    
    /**
     * Get one category
     * 
     * @param string $id
     * @param array $options
     * @return array (_id, name)
     */
    public function searchById($id, array $options = array()) 
    {
        return $this->collection->findOne(array('_id' => new \MongoId($id)));
    }
    
    /**
     * Get all categories
     * 
     * @return \Listing\Category\Dao\ResultSet
     */
    public function getAll()
    {
       $cursor = $this->collection->find(); 
       $cursor->sort(array('name' => 1));
       
       return new ResultSet('', $this->collection, $cursor);
    }

    /**
     * Add one category
     * 
     * @param array $data the data<br />
     * name string the name<br />
     * 
     * @return array
     */
    public function add(array $data)
    {
       $this->collection->insert($data);
       return $data;
    }
}
