<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Category\Dao;

/**
 * Interface for dao for category
 */
interface ITemplate
{ 
    /**
     * Get one category
     * 
     * @param string $id
     * @param array $options
     * @return array (_id, name)
     */
    public function searchById($id, array $options = array());
    
    /**
     * Get all categories
     * 
     * @return array
     */
    public function getAll();
    
    /**
     * Add one category
     * 
     * @param array $data the data<br />
     * name string the name<br />
     * 
     * @return array
     */
    public function add(array $data);
}
