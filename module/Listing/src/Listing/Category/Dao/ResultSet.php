<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Category\Dao;

use Listing\ADaoResultSet;

/**
 * Result set for category
 */
class ResultSet extends ADaoResultSet
{
    
}
