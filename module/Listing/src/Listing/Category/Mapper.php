<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Category;

use Listing\AMapper;

/**
 * Mapper for category
 */
class Mapper extends AMapper
{        
    /**
     *
     * @var string 
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Get the class name
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getClassName()
    {
        return '\Listing\Category';
    }
}
