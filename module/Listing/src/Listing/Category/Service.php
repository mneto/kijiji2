<?php

namespace Listing\Category;
use Listing\Observable,
    Listing\Observer;

class Service implements Service\ITemplate, Observable
{
    /**
     *
     * @var string 
     */
    public static $CLASS = __CLASS__;
    
    /**
     *
     * @var \Listing\Observer[] 
     */
    protected $observers = array();
    
    /**
     *
     * @var \Listing\Category\Dao\ITemplate 
     */
    protected $dao;
    
    /**
     *
     * @var \Listing\Category\Mapper 
     */
    protected $mapper;
    
    /**
     *
     * @var \Listing\Category\Cache 
     */
    protected $cache;
    
    /**
     * 
     * @return \Listing\Category\Dao\ITemplate
     * @codeCoverageIgnore
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * 
     * @param \Listing\Category\Dao\ITemplate $dao
     * @return void
     * @codeCoverageIgnore
     */
    public function setDao(\Listing\Category\Dao\ITemplate $dao)
    {
        $this->dao = $dao;
    }
    
    /**
     * 
     * @return \Listing\Category\Cache
     * @codeCoverageIgnore
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * 
     * @param \Listing\Category\Cache $cache
     * @return void
     * @codeCoverageIgnore
     */
    public function setCache(\Listing\Category\Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Constructor
     * 
     * @param \Listing\Category\Dao\ITemplate $dao<br />
     * @param \Listing\Category\Mapper $mapper the mapper
     * @param \Listing\Category\Cache $cache
     * @return void
     * @codeCoverageIgnore
     */
    function __construct(
            Dao\ITemplate $dao, 
            \Listing\Category\Mapper $mapper,
            \Listing\Category\Cache $cache)
    {
        $this->dao = $dao;
        $this->mapper = $mapper;
        $this->cache = $cache;
    }
    
    /**
     * Get one category
     * 
     * @param string $id
     * @param array $options
     * @return \Listing\Category|null
     */
    public function searchById($id, array $options = array())
    {
        $rc = $this->dao->searchById($id, $options);
        if($rc != null) {
            return $this->mapper->mapToObject($rc);
        } else {
            return $rc;
        }
    }
    
    /**
     * Get all categories
     * 
     * @return \Listing\AResultSet
     */
    public function getAll()
    {
       $list = $this->cache->getList();
       if (!$list) {
           $list = new \Listing\AResultSet($this->dao->getAll(), $this->mapper); 
           $this->cache->saveList($list);
       } 
       return $list;
    }
    
    /**
     * Attach the observer
     * 
     * @param \Listing\Observer $observer
     * @return \Listing\Category\Service
     * @codeCoverageIgnore
     */
    public function attachObserver(Observer $observer)
    {
        $attach = true;
        foreach ($this->observers as $current) {
            if (get_class($observer) == get_class($current)) {
                $attach = false;
                break;
            }
        }
        
        if ($attach) {
            $this->observers[] = $observer;
        }
        
        return $this;
    }

    /**
     * 
     * @param \Listing\Observer $observer
     * @return \Listing\Category\Service
     * @codeCoverageIgnore
     */
    public function detachObserver(Observer $observer)
    {
        foreach ($this->observers as $key => $current) {
            if (get_class($observer) == get_class($current)) {
                unset($this->observers[$key]);
                break;
            }
        }
        return $this;
    }

    /**
     * Notify
     * 
     * @param string $action
     * @return \Listing\Category\Service
     * @codeCoverageIgnore
     */
    public function notify($action)
    {
        foreach ($this->observers as $observer) {
            $observer->update($this, $action);
        }
        
        return $this;
    }
}
