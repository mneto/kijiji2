<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Category\Service;

/**
 * Interface for the category
 */
interface ITemplate
{
    /**
     * Get one category searching by the id
     * 
     * @param string $id
     * @param array $options
     * @return \Listing\Category|null
     */
    public function searchById($id, array $options = array());
    
    /**
     * Get all categories ordered by name
     * @return \Listing\Category\ResultSet List of categories
     */
    public function getAll();
}
