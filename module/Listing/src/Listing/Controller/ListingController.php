<?php
namespace Listing\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ListingController extends AbstractActionController
{   
    /**
     * @var \Listing\Ad\Service
     */
    protected $adService;

    /**
     * Displays the list of categories and a list of ads for each
     *
     * @return array|ViewModel
     */
    public function indexAction()
    {
        $sm = $this->getServiceLocator();
        
        $adService = $sm->get('Listing\Ad\Service');
        if ($adService instanceof \Listing\Ad\Service) {
            $list = $adService->searchByCategory('50d8d372a1f4793fb2c602e7');
            echo get_class($list) . "<br />";
            foreach ($list as $value) {
                var_dump($value);
            }
        }
        exit();
        
        return new ViewModel(array(
            'dump' => print_r($adService, true),
        ));
    }

    /**
     * Displays one category
     */
    public function categoryAction()
    {

    }

    /**
     * Displays one ad
     */
    public function adAction()
    {

    }
}