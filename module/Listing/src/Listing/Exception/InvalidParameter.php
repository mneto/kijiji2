<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\Exception;

/**
 * Invalid parameter
 */
class InvalidParameter extends \Exception
{
}
