<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * The mapper interface so the DAO is mapped to an object and vice versa
 */
interface IMapper
{
    /**
     * Map a DB result to an object
     * 
     * @param array $data
     * @param array $options
     * @return \StdClass
     */
    public function mapToObject(array &$data, array $options = array());
    
    /**
     * @param \StdClass $obj The object to map
     * @return array
     */
    public function mapToDb($obj);
}
