<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * Messenger
 */
class Messenger
{
    /**
     *
     * @var \Zend_Mail 
     */
    protected $mailer;
    
    /**
     * Constructor
     * 
     * @param array $options
     * @codeCoverageIgnore
     */
    public function __construct(array $options)
    {
        $this->mailer = new \Zend_Mail('UTF-8');
        if (isset($options['mail'])) {
            $smtpHost = 'smtp.gmail.com';
            $smtpConf = array(
             'auth' => 'login',
             'ssl' => 'ssl',
             'port' => '465',
             'username' => $options['mail']['username'],
             'password' => $options['mail']['password']
            );
            
            $tr = new \Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
            $this->mailer->setDefaultTransport($tr);
        }
    }
    
    /**
     * Sends a message
     * 
     * @param array $data the data<br />
     * from string email address of the sender<br />
     * to string email address of the recipient<br />
     * subject string the email's suject<br />
     * html string the html version of the text to be sent<br />
     * 
     * @return void
     */
    public function send(array $data)
    {
        $this->mailer->clearFrom();
        $this->mailer->clearRecipients();
        $this->mailer->clearReplyTo();
        $this->mailer->clearSubject();
        
        $this->mailer->setBodyHtml($data['html']);
        $this->mailer->setSubject($data['subject']);
        $this->mailer->setFrom($data['from']);
        $this->mailer->addTo($data['to']);
        $this->mailer->send();
    }
}
