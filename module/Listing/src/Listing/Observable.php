<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

use Listing\Observer;

/**
 * The observable interface
 */
interface Observable
{
    /**
     * Notify the observer
     */
    public function notify($action);
    
    /**
     * Add an observer
     */
    public function attachObserver(Observer $observer);
    
    /**
     * Delete an observer
     */
    public function detachObserver(Observer $observer);
}
