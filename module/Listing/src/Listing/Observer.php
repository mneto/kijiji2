<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

use Listing\Observable;

/**
 * The observer
 */
interface Observer
{
    /**
     * Receive the update from our observable
     */
    public function update(Observable $observable, $action);
}
