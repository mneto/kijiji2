<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing;

/**
 * User entity
 */
class User
{
    /**
     *
     * @var string 
     */
    protected $id;
    
    /**
     *
     * @var string 
     */
    protected $name;
    
    /**
     *
     * @var string 
     */
    protected $email;
    
    /**
     *
     * @var string 
     */
    protected $type;
    
    /**
     *
     * @var string 
     */
    protected $identity;
    
    /**
     *
     * @var string 
     */
    protected $password;
    
    /**
     * Get the id
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the id
     * 
     * @param string $id
     * @return void
     * @codeCoverageIgnore
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the name
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the name
     * 
     * @param string $name
     * @return void
     * @codeCoverageIgnore
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the email
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the email
     * 
     * @param string $email
     * @return void
     * @codeCoverageIgnore
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get the type
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the type
     * 
     * @param string $type
     * @return void
     * @codeCoverageIgnore
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get the identity
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set the identity
     * 
     * @param string $identity
     * @return void
     * @codeCoverageIgnore
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
    }

    /**
     * Get the password
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the password
     * 
     * @param string $password
     * @return void
     * @codeCoverageIgnore
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}
