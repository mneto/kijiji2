<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\User;

/**
 * User dao
 */
class Dao extends \Listing\ADao implements Dao\ITemplate
{
    /**
     * Collection
     */
    const COLLECTION = 'users';
    
    /**
     * Class name
     */
    public static $CLASS = __CLASS__;
    
    /**
     * Constructor
     * 
     * @param \MongoDB $db
     * @codeCoverageIgnore
     */
    function __construct($db)
    {
        parent::__construct($db);
        $this->collection = new \MongoCollection ($this->db, self::COLLECTION);
    }
    
    /**
     * Get one user
     * 
     * @param string $id
     * @param array $options
     * @return array (_id, name, email, password, type, identity)|null
     */
    public function searchById($id, array $options = array())
    {
        return $this->collection->findOne(array('_id' => new \MongoId($id)));
    }
    
    /**
     * Get one user
     * 
     * @param string $email
     * @param array $options
     * @return array (_id, name)|null
     */
    public function searchByEmail($email, array $options = array())
    {
        return $this->collection->findOne(array('email' => $email));
    }
    
    /**
     * Add one user
     * 
     * @param array $data
     * @return array the same but with the _id property set
     */
    public function add(array $data)
    {
       try {
           $rc = $this->collection->insert($data);
           if ($rc) {
               return $data;
           }
       } catch (\MongoCursorException $exception) {
           throw new \Listing\Exception\Db('Database error', 0, $exception);
       }
    }
    
    /**
     * Update one user
     * 
     * @param array $data
     * @param array $options
     * @return array
     */
    public function update(array $data, array $options = array())
    {
        return $this->collection->update(
                array('_id' => new \MongoId($data['_id'])), 
                $data
                );
    }
}
