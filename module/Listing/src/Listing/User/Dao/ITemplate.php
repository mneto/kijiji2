<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\User\Dao;

/**
 * Template for user service
 */
interface ITemplate {
    /**
     * Get one user
     * 
     * @param string $id
     * @param array $options
     * @return array (_id, name)|null
     */
    public function searchById($id, array $options = array());
    
    /**
     * Get one user
     * 
     * @param string $email
     * @param array $options
     * @return array (_id, name)|null
     */
    public function searchByEmail($email, array $options = array());
    
    /**
     * Update one user
     * 
     * @param array $data
     * @param array $options
     * @return array
     */
    public function update(array $data, array $options = array());
    
    /**
     * Add one user
     * 
     * @param array $data
     * @return array the same but with the _id property set
     */
    public function add(array $data);
}
