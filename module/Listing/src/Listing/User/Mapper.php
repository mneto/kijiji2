<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\User;

use Listing\AMapper;

/**
 * Mapper for user
 */
class Mapper extends AMapper
{   
    /**
     * Get the class name
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getClassName()
    {
        return '\Listing\User';
    }
}
