<?php

namespace Listing\User;

class Service implements Service\ITemplate {
    
    /**
     *
     * @var \Listing\User\Dao\ITemplate
     */
    protected $dao;
    
    /**
     *
     * @var \Listing\User\Mapper 
     */
    protected $mapper;
    
    /**
     *
     * @var \Zend\Validator\EmailAddress 
     */
    protected $validator;
    
    /**
     *
     * @var \Zend\Crypt\Password\PasswordInterface
     */
    protected $crypt;
    
    /**
     * 
     * @return \Listing\User\Dao\ITemplate
     * @codeCoverageIgnore
     */
    public function getDao() {
        return $this->dao;
    }

    /**
     * 
     * @param \Listing\User\Dao\ITemplate $dao
     * @codeCoverageIgnore
     */
    public function setDao(\Listing\User\Dao\ITemplate $dao) {
        $this->dao = $dao;
    }
    
    /**
     * 
     * @return \Listing\User\Mapper
     * @codeCoverageIgnore
     */
    public function getMapper() {
        return $this->mapper;
    }

    /**
     * 
     * @param \Listing\User\Mapper $mapper
     * @codeCoverageIgnore
     */
    public function setMapper(\Listing\User\Mapper $mapper) {
        $this->mapper = $mapper;
    }

    /**
     * 
     * @return \Zend\Validator\EmailAddress
     * @codeCoverageIgnore
     */
    public function getValidator() {
        return $this->validator;
    }

    /**
     * 
     * @param \Zend\Validator\EmailAddress $validator
     * @codeCoverageIgnore
     */
    public function setValidator(\Zend\Validator\EmailAddress $validator) {
        $this->validator = $validator;
    }

    /**
     * 
     * @return \Zend\Crypt\Password\PasswordInterface
     * @codeCoverageIgnore
     */
    public function getCrypt() {
        return $this->crypt;
    }

    /**
     * 
     * @param \Zend\Crypt\Password\PasswordInterface $crypt
     * @codeCoverageIgnore
     */
    public function setCrypt(\Zend\Crypt\Password\PasswordInterface $crypt) {
        $this->crypt = $crypt;
    }
        
    /**
     * @param Dao\ITemplate $dao
     * @param Mapper $mapper
     * @param \Zend\Validator\EmailAddress $validator
     * @param \Zend\Crypt\Password\PasswordInterface $crypt
     * @codeCoverageIgnore
     */
    function __construct(\Listing\User\Dao\ITemplate $dao,
            \Listing\User\Mapper $mapper,
            \Zend\Validator\EmailAddress $validator,
            \Zend\Crypt\Password\PasswordInterface $crypt) {
        $this->dao = $dao;
        $this->mapper = $mapper;
        $this->validator = $validator;
        $this->crypt = $crypt;
    }
    
    /**
     * Get one User
     * 
     * @param string $id
     * @param array $options
     * @return \Listing\User|null
     */
    public function searchById($id, array $options = array()) {
        if (!strlen($id)) {
            throw new \Listing\Exception\InvalidParameter('The id informed is not valid');
        }
        
        $rc = $this->dao->searchById($id, $options);
        if($rc != null) {      
            return $this->mapper->mapToObject($rc);
        } else {
            return $rc;
        }
    }
    
    /**
     * Get one User
     * 
     * @param string $email
     * @param array $options
     * @return \Listing\User|null
     */
    public function searchByEmail($email, array $options = array()) {
        if (!$this->validator->isValid($email)) {
            throw new \Listing\Exception\InvalidParameter('The email is invalid');
        }
        
        $rc = $this->dao->searchByEmail($email);
        if ($rc != null) {
            return $this->mapper->mapToObject($rc);
        } else {
            return $rc;
        }
    }
    
    /**
     * Encrypts the password
     * 
     * @param string $password
     * @return string
     */
    public function crypt($password) {
        return $this->crypt->create($password);
    }
    
    /**
     * Verify that the password matches the encrypted one
     * 
     * @param string $password the plaintext password we want to check
     * @param string $encryptedPassword the encrypted password
     * @return boolean true if verified
     */
    public function verifyPassword($password, $encryptedPassword) {
        return $this->crypt->verify($password, $encryptedPassword);
    }
    
    /**
     * Add one user
     * 
     * @param array $data
     * @return \Listing\User The user added
     * @throws \Listing\Exception\Db
     */
    public function add(array $data) {
        $data['password'] = $this->crypt->create($data['password']);
        $rc = $this->dao->add($data);
        if ($rc) {
            return $this->mapper->mapToObject($data);
        }
    }
    
    /**
     * Update one user
     * 
     * @param array $data
     * @return \Listing\User The user updated
     * @throws \Listing\Exception\User
     * @throws \Listing\Exception\Db
     */
    public function update(array $data) {
        // If the array does not contain id throw exception
        if (!isset($data['_id']) || !strlen($data['_id'])) {
            throw new \Listing\Exception\User('Invalid id');
        }
        
        $rc = $this->dao->update($data);
        if ($rc) {
            if ($rc['n'] == 1) {
                return $this->mapper->mapToObject($data);
            } else {
                throw new \Listing\Exception\User('User not found with _id ' . $data['_id']);
            }
        }
    }
}