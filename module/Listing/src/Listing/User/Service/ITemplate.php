<?php
/**
 * Kijiji Clone
 *
 * @copyright Copyright (c) 2005-2013 Bicatu. (http://www.bicatu.com.br)
 */

namespace Listing\User\Service;

/**
 * Template for the user service
 */
interface ITemplate
{
    /**
     * Get one User
     * 
     * @param string $id
     * @param array $options
     * @return \Listing\User|null
     */
    public function searchById($id, array $options = array());
    
    /**
     * Get one User
     * 
     * @param string $email
     * @param array $options
     * @return \Listing\User|null
     */
    public function searchByEmail($email, array $options = array());
    
    /**
     * Add one user
     * 
     * @param array $data
     * @return \Listing\User The user added
     * @throws \Listing\Exception\Db
     */
    public function add(array $data);
    
    /**
     * Update one user
     * 
     * @param array $data
     * @return \Listing\User The user updated
     * @throws \Listing\Exception\User
     * @throws \Listing\Exception\Db
     */
    public function update(array $data);
}
