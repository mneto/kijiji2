<?php

namespace ListingTest;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.0 on 2013-04-28 at 22:32:06.
 */
class AMapperTest extends \ListingTest\Test\AbstractCase
{
    /**
     * @var \Listing\AMapper
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        
        $this->object = $this->getMockForAbstractClass('\Listing\AMapper');
    }
    
    protected function setClassName($className)
    {
        $this->object->expects($this->any())
                ->method('getClassName')
                ->will($this->returnValue($className));
    }

    /**
     * @covers Listing\AMapper::mapToDb
     */
    public function testMapToDbWithoutId()
    {
        $obj = new \Listing\Ad\Auto();
        $obj->setBrand('Honda');
        $obj->setYear(1990);
        
        $this->setClassName('\Listing\Ad\Auto');
        
        $data = $this->object->mapToDb($obj);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('brand', $data);
        $this->assertEquals($obj->getBrand(), $data['brand']);
        $this->assertArrayHasKey('year', $data);
        $this->assertEquals($obj->getYear(), $data['year']);
    }
    
    /**
     * @covers Listing\AMapper::mapToDb
     */
    public function testMapToDbWithId()
    {
        $mongoId = new \MongoId();
        $obj = new \Listing\User();
        $obj->setId((string) $mongoId);
        $obj->setEmail('email@email.com');
        $obj->setIdentity('10101001');
        $obj->setName('Random name');
        $obj->setPassword('shahhshhs');
        $obj->setType('PF');
        
        $this->setClassName('\Listing\User');
        
        $data = $this->object->mapToDb($obj);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('_id', $data);
        $this->assertEquals($obj->getId(), $data['_id']->{'$id'});
    }

    /**
     * @covers Listing\AMapper::mapToObject
     */
    public function testMapToObjectWithoutId()
    {
        $data['brand'] = 'Suzuki';
        $data['year'] = '1999';
        
        $this->setClassName('\Listing\Ad\Auto');
        
        $obj = $this->object->mapToObject($data);
        $this->assertInstanceOf('\Listing\Ad\Auto', $obj);
        $this->assertEquals($data['brand'], $obj->getBrand());
        $this->assertEquals($data['year'], $obj->getYear());
    }
    
    /**
     * @covers Listing\AMapper::mapToObject
     */
    public function testMapToObjectWithId()
    {
        $data['_id'] = new \MongoId('Hash');
        $data['name'] = 'Random name';
        $data['email'] = 'email@email.com';
        $data['identity'] = '01929292';
        $data['password'] = sha1(uniqid() . microtime());
        $data['type'] = 'PF' ;
        
        $this->setClassName('\Listing\User');
        
        $obj = $this->object->mapToObject($data);
        $this->assertInstanceOf('\Listing\User', $obj);
        $this->assertEquals($data['_id']->{'$id'}, $obj->getId());
    }

    /**
     * @covers Listing\AMapper::mapToObject
     * @expectedException \Listing\Exception\InvalidParameter
     */
    public function testMapToObjectWithoutClassNameShouldGenerateException()
    {
        $data['_id'] = new \MongoId('Hash');
        $data['name'] = 'Random name';
        $data['email'] = 'email@email.com';
        $data['identity'] = '01929292';
        $data['password'] = sha1(uniqid() . microtime());
        $data['type'] = 'PF' ;

        $this->setClassName('');

        $obj = $this->object->mapToObject($data);
    }
}
