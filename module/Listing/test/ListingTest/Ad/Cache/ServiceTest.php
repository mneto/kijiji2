<?php

namespace ListingTest\Ad\Cache;
use ListingTest\Test\AbstractCase;

class ServiceTest extends AbstractCase
{
    /**
     *
     * @var \Listing\Ad\Cache\Service
     */
    protected $service;

    public function setUp()
    {
        parent::setUp();

        $adapter = $this->getMock('\Listing\Cache\Adapter', array(), array(), '', FALSE);
        $this->service = new \Listing\Ad\Cache\Service($adapter);
    }
    
    public function tearDown()
    {
        unset($this->service);
        parent::tearDown();
    }

    /**
     * @covers \Listing\Ad\Cache\AStrategy::buildKey
     */
    public function testBuildKey()
    {
        $options = array('status' => 1,
                         'limit'  => 2,
                         'offset' => 3);

        $categoryId = 'C';

        $key = $this->service->buildKey($categoryId, $options);
        $this->assertEquals(\Listing\Ad\Cache\Service::MAIN_TAG . '_C_1_2_3', $key);
    }

    /**
     * @covers \Listing\Ad\Cache\AStrategy::buildKey
     * @expectedException \Listing\Exception\InvalidParameter
     */
    public function testBuildKeyWithoutStatusShouldFail()
    {
        $options = array(
            'limit'  => 2,
            'offset' => 3);

        $categoryId = 'C';

        $key = $this->service->buildKey($categoryId, $options);
    }

    /**
     * @covers \Listing\Ad\Cache\AStrategy::buildKey
     * @expectedException \Listing\Exception\InvalidParameter
     */
    public function testBuildKeyWithoutLimitShouldFail()
    {
        $options = array(
            'status' => 1,
            'offset' => 3);

        $categoryId = 'C';

        $key = $this->service->buildKey($categoryId, $options);
    }

    /**
     * @covers \Listing\Ad\Cache\AStrategy::buildKey
     * @expectedException \Listing\Exception\InvalidParameter
     */
    public function testBuildKeyWithoutOffsetShouldFail()
    {
        $options = array(
            'status' => 1,
            'limit' => 3);

        $categoryId = 'C';

        $key = $this->service->buildKey($categoryId, $options);
    }

    /**
     * @covers \Listing\Ad\Cache\AStrategy::buildListTags
     */
    public function testBuildListTags()
    {
        $categoryId = 'XXXX';
        $listTags = $this->service->buildListTags($categoryId);
        $this->assertEquals(\Listing\Ad\Cache\Service::MAIN_TAG . '_' . $categoryId, $listTags);
    }
}
