<?php

namespace ListingTest\Ad;

class DaoTest extends \ListingTest\Test\Dao
{
    /**
     *
     * @var \Listing\Ad\Dao 
     */
    protected $dao;
    
    public function setUp() {
        parent::setUp();
        $this->dao = new \Listing\Ad\Dao($this->db);        
    }
    
    /**
     * @param array $options List of options to set
     * @return array
     */
    protected function addAd(array $options = array()) {
        $data = array('title' => 'Vendo moto bem legal' . md5(uniqid()),
                      'categoryId' => isset($options['categoryId']) ? $options['categoryId'] : '50d8d372a1f4793fb2c602e7',
                      'userId'     => '50d87081a1f4793fb2c602e2',
                      'status'   => isset($options['status']) ? $options['status'] : \Listing\Ad::STATUS_ENABLED,
                      'price'    => 10.99,
                      'type'     => \Listing\Ad::TYPE_AUTO,
                      'details' => array(
                        'brand'    => 'Honda',
                        'year'     => 2009)
                     );
        
        return $this->dao->add($data);
    }
    
    /**
     * @covers \Listing\Ad\Dao::add
     */
    public function testAd() {
        $rc = $this->addAd();
        $this->assertInternalType('array', $rc);
        $this->assertArrayHasKey('_id', $rc);
    }
    
    /**
     * @covers \Listing\Ad\Dao::delete
     */
    public function testDeleteExistingShouldSuccess() {
        $data = $this->addAd();
        $rc = $this->dao->delete($data['_id']);
        $this->assertTrue($rc);
    }
    
    /**
     * @covers \Listing\Ad\Dao::delete
     */
    public function testDeleteNonExistingShouldFail() {
        $rc = $this->dao->delete(md5(uniqid()));
        $this->assertFalse($rc);
    }
    
    /**
     * @covers \Listing\Ad\Dao::searchById
     */
    public function testSearchByIdWithResult() {
       $data = $this->addAd();
       $rc = $this->dao->searchById($data['_id']);
       $this->assertInternalType('array', $rc);
       $this->assertInstanceOf('\MongoId', $rc['_id']);
       $this->assertEquals($data['_id'], $rc['_id']->{'$id'});
    }
    
    /**
     * @covers \Listing\Ad\Dao::searchById
     */
    public function testSearchByIdWithoutResult() {
       $rc = $this->dao->searchById(md5(uniqid()));
       $this->assertNull($rc);
    }
    
    /**
     * @covers \Listing\Ad\Dao::searchByCategory
     */
    public function testSearchByCategoryWithResult() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd(array('categoryId' => md5(uniqid())));
        
        $rc = $this->dao->searchByCategory($ad1['categoryId']);
        $this->assertInstanceOf('\Listing\Ad\Dao\ResultSet', $rc);
        $this->assertCount(1, $rc);
        foreach($rc as $ad) {
            $this->assertEquals($ad1['categoryId'], $ad['categoryId']);
        }
    }
    
    /**
     * @covers \Listing\Ad\Dao::searchByCategory
     */
    public function testSearchByCategoryWithResultAndCount() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd(array('categoryId' => md5(uniqid())));
        
        $rc = $this->dao->searchByCategory($ad1['categoryId'], array('count' => true));
        $this->assertEquals(1, $rc);
    }
    
    /**
     * @covers \Listing\Ad\Dao::searchByCategory
     */
    public function testSearchByCategoryWithResultAndLimit() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd();
        $ad3 = $this->addAd();
        $ad4 = $this->addAd(array('categoryId' => md5(uniqid())));
        
        $rc = $this->dao->searchByCategory($ad1['categoryId'], array('limit' => 2));
        $this->assertInstanceOf('\Listing\Ad\Dao\ResultSet', $rc);
        $this->assertCount(2, $rc); // Instead of 3
        foreach($rc as $ad) {
            $this->assertEquals($ad1['categoryId'], $ad['categoryId']);
        }
    }
    
    /**
     * @covers \Listing\Ad\Dao::searchByCategory
     */
    public function testSearchByCategoryWithResultAndSkip() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd();
        $ad3 = $this->addAd();
        $ad4 = $this->addAd(array('categoryId' => md5(uniqid())));
        
        $rc = $this->dao->searchByCategory($ad1['categoryId'], array('offset' => 2));
        $this->assertInstanceOf('\Listing\Ad\Dao\ResultSet', $rc);
        $this->assertCount(1, $rc); // Instead of 3
        foreach($rc as $ad) {
            $this->assertEquals($ad1['categoryId'], $ad['categoryId']);
        }
    }
    
    /**
     * @covers \Listing\Ad\Dao::update
     */
    public function testUpdate() {
        $data = $this->addAd();
        
        $data['title'] = 'Vendo moto bem legal' . md5(uniqid());
        $data['details']['brand'] = 'Hundai';
        
        $rc = $this->dao->update($data);
        $this->assertTrue($rc);
    }
    
    /**
     * @covers \Listing\Ad\Dao::update
     */
    public function testUpdateNoExistingShouldFail() {
        $data = $this->addAd();
        
        $data['_id'] = new \MongoId();  // Creating a new Id that does not exist
        
        $rc = $this->dao->update($data);
        $this->assertFalse($rc);
    }

    /**
     * @covers \Listing\Ad\Dao::getNumberOfAdsPerCategory
     */
    public function testGetNumberOfAdsPerCategoryWithoutStatus() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd();
        $ad3 = $this->addAd(array('status' => \Listing\Ad::STATUS_DISABLED));
        $ad4 = $this->addAd(array('categoryId' => md5(uniqid())));

        $mainCategoryId = $ad1['categoryId'];
        $secondaryCategoryId = $ad4['categoryId'];

        $rc = $this->dao->getNumberOfAdsPerCategory();
        $this->assertInternalType('array', $rc);
        $this->assertCount(2, $rc);
        foreach ($rc as $item) {
            $this->assertArrayHasKey('categoryId', $item);
            $this->assertArrayHasKey('numberOfItems', $item);
            if ($item['categoryId'] == $mainCategoryId) {
                $this->assertEquals(3, $item['numberOfItems']);
            } else {
                $this->assertEquals(1, $item['numberOfItems']);
            }
        }
    }

    /**
     * @covers \Listing\Ad\Dao::getNumberOfAdsPerCategory
     */
    public function testGetNumberOfAdsPerCategoryWithStatus() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd();
        $ad3 = $this->addAd(array('status' => \Listing\Ad::STATUS_DISABLED));
        $ad4 = $this->addAd(array('categoryId' => md5(uniqid())));

        $mainCategoryId = $ad1['categoryId'];
        $secondaryCategoryId = $ad4['categoryId'];

        $rc = $this->dao->getNumberOfAdsPerCategory(array('status' => \Listing\Ad::STATUS_ENABLED));
        $this->assertInternalType('array', $rc);
        $this->assertCount(2, $rc);
        foreach ($rc as $item) {
            $this->assertArrayHasKey('categoryId', $item);
            $this->assertArrayHasKey('numberOfItems', $item);
            if ($item['categoryId'] == $mainCategoryId) {
                $this->assertEquals(2, $item['numberOfItems']);
            } else {
                $this->assertEquals(1, $item['numberOfItems']);
            }
        }
    }

    /**
     * @covers \Listing\Ad\Dao::getNumberOfAdsPerCategory
     */
    public function testGetNumberOfAdsPerCategoryWithStatusAndNoResult() {
        $ad1 = $this->addAd();
        $ad2 = $this->addAd();
        $ad3 = $this->addAd();
        $ad4 = $this->addAd(array('categoryId' => md5(uniqid())));

        $mainCategoryId = $ad1['categoryId'];
        $secondaryCategoryId = $ad4['categoryId'];

        $rc = $this->dao->getNumberOfAdsPerCategory(array('status' => \Listing\Ad::STATUS_DISABLED));
        $this->assertNull($rc);
    }
}