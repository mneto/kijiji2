<?php

namespace ListingTest\Ad;
use ListingTest\Test\AbstractCase;

class ServiceTest extends AbstractCase
{
    /**
     *
     * @var \Listing\Ad\Service 
     */
    protected $adService;
    
    /**
     *
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $adDao;
    
    /**
     *
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $userService;
    
    /**
     *
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $categoryService;
    
    /**
     *
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $mapper;
    
    /**
     *
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $cache;
    
    public function setUp()
    {
        
        parent::setUp();
        
        $this->adDao = $this->getMock(
                '\Listing\Ad\Dao', 
                array(), 
                array(), 
                '', 
                FALSE
                );
        
        $this->userService = $this->getMock('\Listing\User\Service', array(), array(), '', FALSE);
        
        $this->categoryService = $this->getMock('\Listing\Category\Service', array(), array(), '', FALSE);
        
        $this->mapper = $this->getMock('\Listing\Ad\Mapper\ITemplate', array(), array(), '', FALSE);
        
        $this->cache = $this->getMock('\Listing\Ad\Cache\Service', array(), array(), '', FALSE);
        
        $this->adService = new \Listing\Ad\Service(
                $this->adDao,
                $this->categoryService,
                $this->userService,
                $this->mapper,
                $this->cache
                );
        
    }
    
    public function tearDown()
    {
        unset($this->adService);
        parent::tearDown();
    }
    
    /**
     * 
     * @return \Listing\Ad
     */
    protected function createAdObject()
    {
        $category = new \Listing\Category();
        $category->setId(1);
        $category->setName('Cars');

        $user = new \Listing\User();
        $user->setId(1);
        $user->setEmail('test@test.com');
        $user->setIdentity('111111');
        $user->setName('Mario');
        $user->setPassword('xxxxxx');
        $user->setType(1);

        $details = new \Listing\Ad\Auto();

        $ad = new \Listing\Ad();
        $ad->setCategory($category);
        $ad->setOwner($user);
        $ad->setDetails($details);

        return $ad;
    }
    
    
    /**
     * @covers \Listing\Ad\Service::add
     */
    public function testAddAnAutoWithSuccess()
    {    
        $adDao = $this->adService->getDao();
        $observer = $this->getMock('\Listing\Observer', array(), array(), '', FALSE);
        $this->adService->attachObserver($observer);
        
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $data = array('title' => 'Vendo moto bem legal',
                      'categoryId' => '50d8d372a1f4793fb2c602e7',
                      'userId'     => '50d87081a1f4793fb2c602e2',
                      'status'   => \Listing\Ad::STATUS_ENABLED,
                      'price'    => 10.99,
                      'type'     => \Listing\Ad::TYPE_AUTO,
                      'details' => array(
                        'brand'    => 'Honda',
                        'year'     => 2009)
                     );
                        
            $response = $this->createAdObject();
            
            $this->mapper->expects($this->once())
                         ->method('mapToObject')
                         ->will($this->returnValue($response));
            
            $adDao->expects($this->once())
                  ->method('add')
                  ->will($this->returnValue(array()));
            
            // I should notify the observer!
            $observer->expects($this->once())
                    ->method('update')
                    ->with(
                            $this->adService,
                            $this->equalTo('add')
                            );
            
            $ad = $this->adService->add($data);
            $this->assertInstanceOf('\Listing\Ad', $ad);
            $this->assertInstanceOf('\Listing\Ad\Auto', $ad->getDetails());
        }    
    }
    
    /**
     * @covers \Listing\Ad\Service::add
     */
    public function testAddWithProblems()
    {      
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $data = array('title' => 'Vendo moto bem legal',
                      'categoryId' => '50d8d372a1f4793fb2c602e7',
                      'userId'     => '50d87081a1f4793fb2c602e2',
                      'status'   => \Listing\Ad::STATUS_ENABLED,
                      'price'    => 10.99,
                      'type'     => \Listing\Ad::TYPE_AUTO,
                      'details' => array(
                        'brand'    => 'Honda',
                        'year'     => 2009)
                     );
                        
            $adDao->expects($this->once())
                  ->method('add')
                  ->will($this->returnValue(null));
            
            $ad = $this->adService->add($data);
            $this->assertNull($ad);
        }    
    }
    
    /**
     * @covers \Listing\Ad\Service::searchById
     */
    public function testSearchByIdWithResult()
    {
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = '50d87081a1f4793fb2c602e2';
            
            $adDao->expects($this->once())
                  ->method('searchById')
                  ->will($this->returnValue(array('id' => $id)));
            
            $response = $this->createAdObject();
            $response->setId($id);
            
            $this->mapper->expects($this->once())
                         ->method('mapToObject')
                         ->will($this->returnValue($response));
            
            $ad = $this->adService->searchById($id);
            $this->assertInstanceOf('\Listing\Ad', $ad);
            $this->assertEquals($id, $ad->getId());
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::searchById
     */
    public function testSearchByIdWithouResult()
    {
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = '50d87081a1f4793fb2c602e2';
            
            $adDao->expects($this->once())
                  ->method('searchById')
                  ->will($this->returnValue(null));
            
            $ad = $this->adService->searchById($id);
            $this->assertNull($ad);
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::searchByCategory
     */
    public function testSearchByCategoryCount()
    {
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $categoryId = '50d87081a1f4793fb2c602e2';
            
            $adDao->expects($this->once())
                  ->method('searchByCategory')
                  ->will($this->returnValue(10));
            
            $numberOfAds = $this->adService->searchByCategory($categoryId, array('count' => 1));
            $this->assertEquals(10, $numberOfAds);
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::searchByCategory
     */
    public function testSearchByCategoryWithResultCacheMiss()
    {
        $adDao = $this->adService->getDao();
        
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $categoryId = '50d87081a1f4793fb2c602e2';
                        
            $resultSet = $this->getMock('\Listing\Ad\Dao\ResultSet', array(), array(), '', FALSE); 
            $resultSet->expects($this->once())
                      ->method('count')
                      ->will($this->returnValue(1));
            
            $this->mockIterator($resultSet, array(array(1)), 2);
            
            $this->mapper->expects($this->once())
                   ->method('mapToObject')
                   ->will($this->returnValue($this->createAdObject()));
            
            $adDao->expects($this->once())
                  ->method('searchByCategory')
                  ->will($this->returnValue($resultSet));
            
            // We shall call the cache to get 
            $this->cache->expects($this->once())
                    ->method('getList')
                    ->will($this->returnValue(false));
            
            $this->cache->expects($this->once())
                    ->method('saveList');
            
            $ads = $this->adService->searchByCategory($categoryId);
            $this->assertInstanceOf('\Listing\AResultSet', $ads);
            $this->assertCount(1, $ads);
            foreach ($ads as $ad) {
               $this->assertInstanceOf('\Listing\Ad', $ad);
            }
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::searchByCategory
     */
    public function testSearchByCategoryWithResultCacheHit()
    {
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $categoryId = '50d87081a1f4793fb2c602e2';
            
            $list = array($this->createAdObject());
            $cacheResult = new \Listing\ACacheResultSet($list);
            
            $this->cache->expects($this->once())
                    ->method('getList')
                    ->will($this->returnValue($cacheResult));
            
            $ads = $this->adService->searchByCategory($categoryId);
            $this->assertInstanceOf('\Listing\ACacheResultSet', $ads);
            $this->assertCount(1, $ads);
            foreach ($ads as $ad) {
               $this->assertInstanceOf('\Listing\Ad', $ad);
            }
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::searchByCategory
     */
    public function testSearchByCategoryWithNoResult()
    {
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $categoryId = '50d87081a1f4793fb2c602e2';
                        
            $adDao->expects($this->once())
                  ->method('searchByCategory')
                  ->will($this->returnValue(NULL));
            
            $this->cache->expects($this->once())
                    ->method('getList')
                    ->will($this->returnValue(false));
            
            $ads = $this->adService->searchByCategory($categoryId);
            $this->assertNull($ads);
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::getNumberOfAdsPerCategory
     */
    public function testGetNumberOfAdsPerCategory()
    {
        $adDao = $this->adService->getDao();
        if ($adDao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $options = array('status' => \Listing\Ad::STATUS_ENABLED);
            
            $daoResult = array(
                array('categoryId'    => 1, 
                    'numberOfItems' => 10),
                array('categoryId'    => 1,
                    'numberOfItems' => 10)
                );
            
            $adDao->expects($this->once())
                  ->method('getNumberOfAdsPerCategory')
                  ->will($this->returnValue($daoResult));
            
            
            $categoryServiceResult = array(array('category'      => new \Listing\Category(),
                                                 'numberOfItems' => 10));
            
            $categoryService = $this->adService->getCategoryService();
            $categoryService->expects($this->any())
                            ->method('searchById')
                            ->will($this->returnValue($categoryServiceResult));
            
            $rc = $this->adService->getNumberOfAdsPerCategory($options);
            $this->assertInternalType('array', $rc);
            
        }
    }
    
    /**
     * @covers \Listing\Ad\Service::update
     */
    public function testUpdateWithSuccess() {
        // Our stub observer
        $observer = $this->getMock('\Listing\Observer', array(), array(), '', FALSE);
        $this->adService->attachObserver($observer);
        
        $this->adDao->expects($this->once())
                ->method('update')
                ->will($this->returnValue(true));
        
        $this->mapper->expects($this->once())
                ->method('mapToDb')
                ->will($this->returnValue(array()));
        
        $observer->expects($this->once())
                ->method('update')
                ->with($this->adService,
                       $this->equalTo('update'));
        
        $ad = $this->createAdObject();
        $ad->setId('1');
        $rc = $this->adService->update($ad);
        $this->assertTrue($rc);
    }
    
    /**
     * @covers \Listing\Ad\Service::update
     */
    public function testUpdateWithoutIdShouldFail() { 
        $ad = $this->createAdObject();
        $ad->setId(null);
        $rc = $this->adService->update($ad);
        $this->assertFalse($rc);
    }
    
    /**
     * @covers \Listing\Ad\Service::delete
     */
    public function testDeleteExisting() {
        $ad = $this->createAdObject();
        $ad->setId(1);
        
        $this->adDao->expects($this->once())
                ->method('delete')
                ->will($this->returnValue(true));
        
        $this->assertTrue($this->adService->delete($ad->getId()));
    }
    
    /**
     * @covers \Listing\Ad\Service::delete
     */
    public function testDeleteInvalidIdShouldFail() {
        $ad = $this->createAdObject();
        $ad->setId(1);
                
        $this->assertFalse($this->adService->delete(NULL));
    }
    
    /**
     * @covers \Listing\Ad\Service::delete
     */
    public function testDeleteNonExistingIdShouldFail() {
        $ad = $this->createAdObject();
        $ad->setId(1);
        
        $this->adDao->expects($this->once())
                ->method('delete')
                ->will($this->returnValue(false));
        
        $this->assertFalse($this->adService->delete($ad->getId()));
    }

    /**
     * @covers \Listing\Ad\Service::attachObserver
     */
    public function testAttachObserver()
    {
        $observer = $this->getMock('\Listing\Ad\Cache\AStrategy', array(), array(), '', FALSE);
        $this->adService->attachObserver($observer);
        $observers = $this->adService->getObservers();
        $this->assertCount(1, $observers);
        $this->assertEquals($observer, $observers[0]);
    }

    /**
     * @covers \Listing\Ad\Service::attachObserver
     */
    public function testAttachObserverTwoTimesShouldNotAdd()
    {
        $observer = $this->getMock('\Listing\Ad\Cache\AStrategy', array(), array(), '', FALSE);
        $this->adService->attachObserver($observer);

        $newObserver = clone $observer;
        $this->adService->attachObserver($newObserver);

        $observers = $this->adService->getObservers();
        $this->assertCount(1, $observers);
        $this->assertEquals($observer, $observers[0]);
    }

    /**
     * @covers \Listing\Ad\Service::detachObserver
     */
    public function testDetachObserver()
    {
        $observer = $this->getMock('\Listing\Ad\Cache\AStrategy', array(), array(), '', FALSE);
        $this->adService->attachObserver($observer);

        $this->adService->detachObserver($observer);
        $observers = $this->adService->getObservers();
        $this->assertCount(0, $observers);
    }

    /**
     * @covers \Listing\Ad\Service::notify
     */
    public function testNotify()
    {
        $observer = $this->getMock('\Listing\Ad\Cache\AStrategy', array(), array(), '', FALSE);
        $observer->expects($this->once())
            ->method('update');

        $this->adService->attachObserver($observer);
        $this->adService->notify('something');
    }
}
