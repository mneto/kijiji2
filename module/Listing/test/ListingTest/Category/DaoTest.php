<?php

namespace ListingTest\Category;

class DaoTest extends \ListingTest\Test\Dao
{
    /**
     *
     * @var \Listing\Category\Dao 
     */
    protected $dao;
    
    public function setUp() {
        parent::setUp();
        $this->dao = new \Listing\Category\Dao($this->db);
    }
    
    /**
     * @param array $options List of options to set
     * @return array
     */
    protected function addCategory(array $options = array()) {
        $data = array('name' => isset($options['name']) ? 
            $options['name'] : 'Category ' . md5(uniqid())
                     );
        
        return $this->dao->add($data);
    }

    /**
     * @covers \Listing\Category\Dao::add
     */
    public function testAddCategory()
    {
        $rc = $this->addCategory();
        $this->assertInternalType('array', $rc);
        $this->assertArrayHasKey('_id', $rc);
    }

    /**
     * @covers \Listing\Category\Dao::searchById
     */
    public function testSearchById() {
        $data = $this->addCategory();
        $id = $data['_id']->{'$id'};
        $rc = $this->dao->searchById($id);
        $this->assertInternalType('array', $rc);
        $this->arrayHasKey('_id', $rc);
    }
    
    /**
     * @covers \Listing\Category\Dao::searchById
     */
    public function testSearchByIdWithNoResult() {
        $this->addCategory();
        $rc = $this->dao->searchById(md5(uniqid()));
        $this->assertNull($rc);
    }
    
    /**
     * @covers \Listing\Category\Dao::getAll
     */
    public function testGetAll() {
        $category1 = $this->addCategory();
        $category2 = $this->addCategory();
        
        $rc = $this->dao->getAll();
        $this->assertInstanceOf('\Listing\Category\Dao\ResultSet', $rc);
        $this->assertCount(2, $rc);
    }
}