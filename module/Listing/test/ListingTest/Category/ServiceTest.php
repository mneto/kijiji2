<?php

namespace ListingTest\Category;
use ListingTest\Test\AbstractCase;

class ServiceTest extends AbstractCase
{
    /**
     *
     * @var \Listing\Category\Service 
     */
    protected $categoryService;
    
    /**
     *
     * @var PHPUnit_Framework_MockObject_MockObject 
     */
    protected $categoryDao;
    
    /**
     *
     * @var PHPUnit_Framework_MockObject_MockObject 
     */
    protected $mapper;
    
    public function setUp()
    {
        
        parent::setUp();

        
        $this->categoryDao = $this->getMock(
                '\Listing\Category\Dao',
                array(),
                array(),
                '',
                FALSE
                );
                
        $this->mapper = $this->getMock(
                '\Listing\Category\Mapper',
                array(),
                array(),
                '',
                FALSE
                );
        
        $this->categoryService = new \Listing\Category\Service(
                $this->categoryDao,
                $this->mapper,
                $this->getMock(
                        '\Listing\Category\Cache',
                        array(),
                        array(),
                        '',
                        FALSE
                        )
                );
        
    }
    
    public function tearDown()
    {
        unset($this->categoryService);
        parent::tearDown();
    }
    
    /**
     * 
     * @return \Listing\Category
     */
    protected function createCategoryObject()
    {
        $category = new \Listing\Category();
        $category->setId(1);
        $category->setName('Cars');

        return $category;
    }
    
    /**
     * @covers \Listing\Category\Service::searchById
     */
    public function testSearchById()
    {
        $dao = $this->categoryService->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = '50d87081a1f4793fb2c602e2';
            
            $dao->expects($this->once())
                  ->method('searchById')
                  ->will($this->returnValue(array('id' => $id)));
            
            $response = $this->createCategoryObject();
            $response->setId($id);
            
            $this->mapper->expects($this->once())
                         ->method('mapToObject')
                         ->will($this->returnValue($response));
            
            $category = $this->categoryService->searchById($id);
            $this->assertInstanceOf('\Listing\Category', $category);
            $this->assertEquals($id, $category->getId());
        }
    }

    /**
     * @covers \Listing\Category\Service::searchById
     */
    public function testSearchByIdWithoutResultShouldReturnNull()
    {
        $dao = $this->categoryService->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = md5(uniqid());

            $dao->expects($this->once())
                ->method('searchById')
                ->will($this->returnValue(null));

            $category = $this->categoryService->searchById($id);
            $this->assertNull($category);
        }
    }
    
    /**
     * @covers \Listing\Category\Service::getAll
     */
    public function testGetAllWithCacheMiss()
    {
        $dao = $this->categoryService->getDao();
        $category = $this->createCategoryObject();
        
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $resultSet = $this->getMock('\Listing\Category\Dao\ResultSet', array(), array(), '', FALSE); 
            $resultSet->expects($this->once())
                      ->method('count')
                      ->will($this->returnValue(1));
            
            $this->mockIterator($resultSet, array(array(1)), 2);
            
            $this->mapper->expects($this->once())
                   ->method('mapToObject')
                   ->will($this->returnValue($category));
            
            $dao->expects($this->once())
                  ->method('getAll')
                  ->will($this->returnValue($resultSet));
            
        }
        
        $result = $this->categoryService->getAll();
        $this->assertInstanceOf('\Listing\AResultSet', $result);
        $this->assertCount(1, $result);
        foreach ($result as $newCategory) {
            $this->assertEquals($category->getName(), $newCategory->getName());
        }
    }
    
    /**
     * @covers \Listing\Category\Service::getAll
     */
    public function testGetAllWithCacheHit()
    {
        $cache = $this->categoryService->getCache();
        if ($cache instanceof \PHPUnit_Framework_MockObject_MockObject) {
            
            $category = $this->createCategoryObject();
            $list = array($category);
            
            $cachedResultSet = new \Listing\ACacheResultSet($list);
            
            $cache->expects($this->once())
                    ->method('getList')
                    ->will($this->returnValue($cachedResultSet));
            
        }
        
        $result = $this->categoryService->getAll();
        $this->assertInstanceOf('\Listing\ACacheResultSet', $result);
        $this->assertCount(1, $result);
        foreach ($result as $newCategory) {
            $this->assertEquals($category->getName(), $newCategory->getName());
        }
    }
}