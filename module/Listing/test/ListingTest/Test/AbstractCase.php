<?php

namespace ListingTest\Test;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class AbstractCase extends AbstractHttpControllerTestCase
{    
    /**
     * setup
     */
    public function setUp() {
        
        $this->setApplicationConfig(
            include dirname(__FILE__) . '/../../../../../config/application.config.php'
        );
        
        parent::setUp();
    }
    
    /**
     * Mock iterator
     *
     * This attaches all the required expectations in the right order so that
     * our iterator will act like an iterator!
     *
     * @param Iterator $iterator The iterator object; this is what we attach
     *      all the expectations to
     * @param array An array of items that we will mock up, we will use the
     *      keys (if needed) and values of this array to return
     * @param boolean $includeCallsToKey Whether we want to mock up the calls
     *      to "key"; only needed if you are doing foreach ($foo as $k => $v)
     *      as opposed to foreach ($foo as $v)
     */
    protected function mockIterator(
            \Iterator $iterator,
            array $items,
            $position = 1,
            $includeCallsToKey = FALSE
            )
    {
        $iterator->expects($this->at(0))
                 ->method('rewind');
        $counter = $position;
        foreach ($items as $k => $v)
        {
            $iterator->expects($this->at($counter++))
                     ->method('valid')
                     ->will($this->returnValue(TRUE));
            $iterator->expects($this->at($counter++))
                     ->method('current')
                     ->will($this->returnValue($v));
            if ($includeCallsToKey)
            {
                $iterator->expects($this->at($counter++))
                         ->method('key')
                         ->will($this->returnValue($k));
            }
            $iterator->expects($this->at($counter++))
                     ->method('next');
        }
        $iterator->expects($this->at($counter))
                 ->method('valid')
                 ->will($this->returnValue(FALSE));
    }
}