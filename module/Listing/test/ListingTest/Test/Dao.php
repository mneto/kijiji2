<?php

namespace ListingTest\Test;
use ListingTest\Test\AbstractCase;

class Dao extends AbstractCase
{
    /**
     *
     * @var \MongoDB 
     */
    protected $db;
    
    /**
     * setup
     */
    public function setUp() {
        parent::setUp();
                
        $mongo = new \MongoClient();
        $this->db = $mongo->selectDB('kijiji2');
    }
    
    /**
     * teardown
     */
    public function tearDown() {
        $collectionNames = $this->db->getCollectionNames();
        foreach ($collectionNames as $collectionName) {
            $collection = new \MongoCollection($this->db, $collectionName);
            $collection->remove();
        }
        parent::tearDown();
    }
}