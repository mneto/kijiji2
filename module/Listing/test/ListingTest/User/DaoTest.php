<?php

namespace ListingTest\User;

class DaoTest extends \ListingTest\Test\Dao
{
    /**
     *
     * @var \Listing\User\Dao 
     */
    protected $dao;
    
    public function setUp() {
        parent::setUp();
        $this->dao = new \Listing\User\Dao($this->db);
    }
    
    /**
     * @param array $options List of options to set
     * @return array
     */
    protected function addUser(array $options = array()) {
        $data = array(
            'name' => isset($options['name']) ? 
            $options['name'] : 'Name ' . md5(uniqid()),
            'email' => isset($options['email']) ? 
            $options['email'] : 'email' . md5(uniqid()) . '@email.com',
            'type' => isset($options['type']) ? 
            $options['type'] : 'type' . md5(uniqid()),
            'identity' => isset($options['identity']) ? 
            $options['identity'] : 'identify' . md5(uniqid()),
                     );
        
        $user = $this->dao->add($data);
        
        $this->dao->getCollection()->ensureIndex(array('email' => 1), array('unique' => 1));
        
        return $user;
    }
    
    /**
     * @covers \Listing\User\Dao::searchById
     */
    public function testSearchById() {
        $user = $this->addUser();
        $id = $user['_id']->{'$id'};
        $rc = $this->dao->searchById($id);
        $this->assertInternalType('array', $rc);
        $this->assertArrayHasKey('_id', $rc);
        $this->assertEquals($id, $rc['_id']->{'$id'});
    }
    
    /**
     * @covers \Listing\User\Dao::searchById
     */
    public function testSearchByIdWithNoResult() {
        $this->addUser();
        $id = uniqid();
        $rc = $this->dao->searchById($id);
        $this->assertNull($rc);
    }
    
    /**
     * @covers \Listing\User\Dao::searchByEmail
     */
    public function testSearchByEmail() {
        $user = $this->addUser();
        $email = $user['email'];
        $rc = $this->dao->searchByEmail($email);
        $this->assertInternalType('array', $rc);
        $this->assertArrayHasKey('_id', $rc);
        $this->assertEquals($email, $rc['email']);
    }
    
    /**
     * @covers \Listing\User\Dao::searchByEmail
     */
    public function testSearchByEmailWithNoResult() {
        $this->addUser();
        $email = uniqid();
        $rc = $this->dao->searchByEmail($email);
        $this->assertNull($rc);
    }
    
    /**
     * @covers \Listing\User\Dao::add
     */
    public function testAdd() {
        $user = $this->addUser();
        $this->assertInternalType('array', $user);
    }
    
    /**
     * @covers \Listing\User\Dao::add
     * @expectedException \Listing\Exception\Db
     */
    public function testAddDuplicateEmailShouldFail() {
        $user = $this->addUser();
        
        // Now try to add it again (remove the id)
        unset($user['_id']);
        $this->dao->add($user);
    }
    
    /**
     * @covers \Listing\User\Dao::update
     */
    public function testUpdate() {
        $user = $this->addUser();
        $user['name'] = uniqid();
        $rc = $this->dao->update($user);
        $this->assertInternalType('array', $rc);
        $this->assertArrayHasKey('updatedExisting', $rc);
        $this->assertEquals(true, $rc['updatedExisting']);
        $this->assertArrayHasKey('n', $rc);
        $this->assertEquals(1, $rc['n']);
    }
}