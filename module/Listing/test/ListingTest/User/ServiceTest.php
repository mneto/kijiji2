<?php

namespace ListingTest\User;
use ListingTest\Test\AbstractCase;

class ServiceTest extends AbstractCase
{
    /**
     *
     * @var \Listing\User\Service 
     */
    protected $service;
    
    /**
     *
     * @var PHPUnit_Framework_MockObject_MockObject 
     */
    protected $dao;
    
    /**
     *
     * @var PHPUnit_Framework_MockObject_MockObject 
     */
    protected $mapper;
    
    public function setUp() {
        
        parent::setUp();

        
        $this->dao = $this->getMock(
                '\Listing\User\Dao', 
                array(), 
                array(), 
                '', 
                FALSE
                );
                
        $this->mapper = $this->getMock(
                '\Listing\User\Mapper', 
                array(), 
                array(), 
                '', 
                FALSE
                );
        
        $this->service = new \Listing\User\Service(
                $this->dao, 
                $this->mapper, 
                new \Zend\Validator\EmailAddress(),
                new \Zend\Crypt\Password\Bcrypt()
                );
        
    }
    
    public function tearDown() {
        unset($this->service);
        parent::tearDown();
    }
    
    /**
     * 
     * @param array $data options to populate
     * @return \Listing\User
     */
    protected function createUserObject(array $data=array()) {
        $user = new \Listing\User();
        
        if (isset($data['email'])) {
            $user->setEmail($data['email']);            
        } else {
            $user->setEmail('user' . md5(uniqid()) . '@email.com');
        }

        if (isset($data['id'])) {
            $user->setId($data['id']);
        } else {
            $user->setId(1);
        }
        
        if (isset($data['identify'])) {
            $user->setIdentity($data['identify']);
        } else {
            $user->setIdentity(md5(uniqid()));
        }
        
        if (isset($data['name'])) {
            $user->setName($data['name']);
        } else {
            $user->setName('name ' . md5(uniqid()));
        }
        
        if (isset($data['password'])) {
            $user->setPassword($data['password']);
        } else {
            $user->setPassword(sha1(md5(uniqid())));
        }
        
        if (isset($data['type'])) {
            $user->setType($data['type']);
        } else {
            $user->setType('PF');
        }

        return $user;
    }
    
    /**
     * @covers \Listing\User\Service::searchById
     */
    public function testSearchById() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = '1111111';
            
            $user = $this->createUserObject();
            $user->setId($id);
            
            $dao->expects($this->once())
                    ->method('searchById')
                    ->will($this->returnValue(array(1,2,3)));
            
            $mapper = $this->service->getMapper();
            $mapper->expects($this->once())
                    ->method('mapToObject')
                    ->will($this->returnValue($user));
            
            $rc = $this->service->searchById($id);
            $this->assertInstanceOf('\Listing\User', $rc);
            $this->assertEquals($id, $rc->getId());
        }
    }
    
    /**
     * @covers \Listing\User\Service::searchById
     */
    public function testSearchByIdWithoutResult() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = '1111111';
            
            $dao->expects($this->once())
                    ->method('searchById')
                    ->will($this->returnValue(null));
                        
            $rc = $this->service->searchById($id);
            $this->assertNull($rc);
        }
    }
    
    /**
     * @covers \Listing\User\Service::searchById
     * @expectedException \Listing\Exception\InvalidParameter
     */
    public function testSearchByIdWithoutIdShouldFail() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $id = null;
            
            $rc = $this->service->searchById($id);
            $this->assertNull($rc);
        }
    }
    
    /**
     * @covers \Listing\User\Service::searchByEmail
     */
    public function testSearchByEmail() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $email = 'test@email.com';
            
            $user = $this->createUserObject();
            $user->setEmail($email);
            
            $dao->expects($this->once())
                    ->method('searchByEmail')
                    ->will($this->returnValue(array(1,2,3)));
            
            $mapper = $this->service->getMapper();
            $mapper->expects($this->once())
                    ->method('mapToObject')
                    ->will($this->returnValue($user));
            
            $rc = $this->service->searchByEmail($email);
            $this->assertInstanceOf('\Listing\User', $rc);
            $this->assertEquals($email, $rc->getEmail());
        }
    }
    
    /**
     * @covers \Listing\User\Service::searchByEmail
     */
    public function testSearchByEmailWithoutResult() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $email = 'test@email.com';
            
            $dao->expects($this->once())
                    ->method('searchByEmail')
                    ->will($this->returnValue(null));
                        
            $rc = $this->service->searchByEmail($email);
            $this->assertNull($rc);
        }
    }
    
    /**
     * @covers \Listing\User\Service::searchByEmail
     * @expectedException \Listing\Exception\InvalidParameter
     */
    public function testSearchByEmailWithInvalidEmailShouldFail() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $email = 'testemail.com';
            
            $rc = $this->service->searchByEmail($email);
        }
    }
    
    /**
     * @covers \Listing\User\Service::add
     */
    public function testAdd() {
        $data = array(
            'name'  => 'Mario',
            'email' => 'mbneto@gmail.com',
            'identity' => 'xxxxxxx',
            'password' => 'test',
            'type'     => 'PF'
        );
        
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $dao->expects($this->once())
                    ->method('add')
                    ->will($this->returnValue($data));
            
            $user = $this->createUserObject($data);
            
            $mapper = $this->service->getMapper();
            $mapper->expects($this->once())
                    ->method('mapToObject')
                    ->will($this->returnValue($user));
        }
        
        $rc = $this->service->add($data);
        $this->assertInstanceOf('\Listing\User', $rc);
    }
    
    /**
     * @covers \Listing\User\Service::add
     * @expectedException \Listing\Exception\Db
     */
    public function testAddDuplicateEmailShouldFail() {
        $data = array(
            'name'  => 'Mario',
            'email' => 'mbneto@gmail.com',
            'identity' => 'xxxxxxx',
            'password' => 'test',
            'type'     => 'PF'
        );
        
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $dao->expects($this->at(0))
                    ->method('add')
                    ->will($this->returnValue(array(1,2,3)));
            
            $dao->expects($this->at(1))
                    ->method('add')
                    ->will($this->throwException(new \Listing\Exception\Db()));            
        }
        
        $rc = $this->service->add($data);
        $rc = $this->service->add($data);
    }
    
    /**
     * @covers \Listing\User\Service::update
     */
    public function testUpdate() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $data = array(
            '_id' => md5(uniqid()),
            'name'  => 'Mario',
            'email' => 'mbneto@gmail.com',
            'identity' => 'xxxxxxx',
            'password' => 'test',
            'type'     => 'PF'
            );
            
            $user = $this->createUserObject($data);
            
            $dao->expects($this->once())
                    ->method('update')
                    ->will($this->returnValue(array('ok' => 1, 'n' => 1)));
            
            $mapper = $this->service->getMapper();
            $mapper->expects($this->once())
                    ->method('mapToObject')
                    ->will($this->returnValue($user));
        }
        
        $rc = $this->service->update($data);
        $this->assertInstanceOf('\Listing\User', $rc);
    }
    
    /**
     * @covers \Listing\User\Service::update
     * @expectedException \Listing\Exception\User
     */
    public function testUpdateWithoutIdShouldFail() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $data = array(
            'name'  => 'Mario',
            'email' => 'mbneto@gmail.com',
            'identity' => 'xxxxxxx',
            'password' => 'test',
            'type'     => 'PF'
            );
        }
        
        $this->service->update($data);
    }
    
        /**
     * @covers \Listing\User\Service::update
     * @expectedException \Listing\Exception\User
     */
    public function testUpdateNonExistingUserIdShouldFail() {
        $dao = $this->service->getDao();
        if ($dao instanceof \PHPUnit_Framework_MockObject_MockObject) {
            $data = array(
            '_id' => md5(uniqid()),
            'name'  => 'Mario',
            'email' => 'mbneto@gmail.com',
            'identity' => 'xxxxxxx',
            'password' => 'test',
            'type'     => 'PF'
            );
            
            $dao->expects($this->once())
                    ->method('update')
                    ->will($this->returnValue(array('ok' => 1, 'n' => 0)));
            
        }
        
        $this->service->update($data);
    }
    
    /**
     * @covers \Listing\User\Service::crypt
     */
    public function testCrypt() {
        $password = 'x';
        $rc = $this->service->crypt($password);
        $this->assertEquals(60, strlen($rc));
    }
    
    /**
     * @covers \Listing\User\Service::verifyPassword
     */
    public function testVerifyEqualPasswordsShouldPass() {
        $password = 'x';
        $hash = '$2y$14$ygHpJ9edfQbUfq5VGGszguuiFpQrnj0ztvHi/vGYRCV.nbdixLxDS';
        $this->assertTrue($this->service->verifyPassword($password, $hash));
    }
    
    /**
     * @covers \Listing\User\Service::verifyPassword
     */
    public function testVerifyDiferentPasswordsShouldPass() {
        $password = 'y';
        $hash = '$2y$14$ygHpJ9edfQbUfq5VGGszguuiFpQrnj0ztvHi/vGYRCV.nbdixLxDS';
        $this->assertFalse($this->service->verifyPassword($password, $hash));
    }
}